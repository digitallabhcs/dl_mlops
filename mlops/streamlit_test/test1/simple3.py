import streamlit as st
# To make things easier later, we're also importing numpy and pandas for
# working with sample data.
import numpy as np
import pandas as pd
import time

# https://towardsdatascience.com/build-your-first-interactive-data-science-web-app-with-streamlit-d4892cbe4792
# https://towardsdatascience.com/data-apps-with-pythons-streamlit-b14aaca7d083

st.set_page_config(page_title='Streamlit Sandbox', page_icon=':memo:', layout='wide', initial_sidebar_state='collapsed')
st.sidebar.title("Choose an instance")

left, right = st.beta_columns((1, 1))


@st.cache
def load_dataset(data_link):
    dataset = pd.read_csv(data_link)
    return dataset


with left:
    titanic_link = 'https://raw.githubusercontent.com/mwaskom/seaborn-data/master/titanic.csv'
    titanic_data = load_dataset(titanic_link)
    st.dataframe(titanic_data)
    left.header('### Left Panel')
    st.write('### Left Panel')

with right:
    right.header('### Right Panel')
    right.subheader('subheader')
    st.markdown('# Just like a subheader')
    st.write('### Right Panel')
    selected_class = st.radio("Select Class", titanic_data['class'].unique())
    st.write("Selected Class:", selected_class)
    st.write("Selected Class Type:", type(selected_class))
    selected_sex = st.selectbox("Select Sex", titanic_data['sex'].unique())
    st.write(f"Selected Option: {selected_sex!r}")
    selected_decks = st.multiselect("Select Decks", titanic_data['deck'].unique())
    st.write("Selected Decks:", selected_decks)


c1, c2, c3, c4 = st.beta_columns((2, 1, 1, 1))

with c1:
    st.write('c1')
with c2:
    st.write('c2')
with c3:
    st.write('c3')
with c4:
    st.write('c4')


with st.sidebar:
    libraries_available = st.beta_expander('Available Libraries')
    with libraries_available:
        st.write("""
            * Pandas (pd)
            * Numpy (np)
            * Altair (alt)
            * Bokeh
            * Plotly
            * Cufflinks (cf)
            [Need something else?](https://github.com/samdobson/streamlit-sandbox/issues/new)
            """)
    my_expander = st.beta_expander()
    my_expander.write('Hello there!')
    clicked = my_expander.button('Click me!')
