import streamlit as st
from bokeh.models import Div
import os
# To make things easier later, we're also importing numpy and pandas for
# working with sample data.
import numpy as np
import pandas as pd
import time

#st.text_input("Password:", value="", type="password")
password = st.sidebar.text_input("Password:", value="", type="password")

# select our text input field and make it into a password input
js = "el = document.querySelectorAll('.sidebar-content input')[0]; el.type = 'password';"

# passing js code to the onerror handler of an img tag with no src
# triggers an error and allows automatically running our code
html = f'<img src onerror="{js}">'

# in contrast to st.write, this seems to allow passing javascript
div = Div(text=html)
st.bokeh_chart(div)

if password != os.environ["PASSWORD"]:
    st.error("the password you entered is incorrect")
