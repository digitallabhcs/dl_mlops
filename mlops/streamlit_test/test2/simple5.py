import streamlit as st
from streamlit_metrics import metric, metric_row

st.write('## Here is a single figure')
metric("Metric 0", 0)

st.write('## ... and here is a row of them')
metric_row(
    {
        "Metric 1": 100,
        "Metric 2": 200,
        "Metric 3": 300,
        "Metric 4": 400,
        "Metric 5": 500,
    }
)