import pandas as pd



def write_to_html_file(df, title='', filename='out.html'):
    '''
    Write an entire dataframe to an HTML file with nice formatting.
    '''

    result = '''
<html>
<head>
<style>

    h2 {
        text-align: center;
        font-family: Helvetica, Arial, sans-serif;
    }
    table { 
        margin-left: auto;
        margin-right: auto;
        font-family: "Source Sans Pro";
        font-weight: 400;
        font-style: normal;
        font-size: 14px;
    }
    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
    }
    th, td {
        padding: 5px;
        text-align: center;
        font-family: Helvetica, Arial, sans-serif;
        font-size: 90%;
    }
    table tbody tr:hover {
        background-color: #dddddd;
    }
    .wide {
        width: 90%; 
    }
</style>
</head>
<body>
    '''
    result += '<h2> %s </h2>\n' % title
    result += df.to_html()
    result += '''
</body>
</html>
'''
    with open(filename, 'w') as f:
        f.write(result)