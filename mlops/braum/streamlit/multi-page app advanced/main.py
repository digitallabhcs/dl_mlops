import streamlit as st
from multiapp import MultiApp
from apps import app1, app2

app = MultiApp()

# Add all your application here
app.add_app("Home", app1.app)
app.add_app("Data Stats", app2.app)

# The main app
app.run()