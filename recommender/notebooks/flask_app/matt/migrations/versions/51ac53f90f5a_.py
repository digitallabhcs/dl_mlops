"""empty message

Revision ID: 51ac53f90f5a
Revises: 7017b972a005
Create Date: 2021-08-10 19:27:50.206137

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '51ac53f90f5a'
down_revision = '7017b972a005'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('useritemmodel',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('user_id', sa.String(length=150), nullable=False),
    sa.Column('item_id', sa.String(length=200), nullable=False),
    sa.Column('model_a', sa.String(length=120), nullable=False),
    sa.Column('model_b', sa.String(length=120), nullable=False),
    sa.Column('model_c', sa.String(length=120), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('user_id')
    )
    op.drop_table('compare')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('compare',
    sa.Column('id', sa.INTEGER(), nullable=False),
    sa.Column('user_id', sa.VARCHAR(length=150), nullable=False),
    sa.Column('item_id', sa.VARCHAR(length=200), nullable=False),
    sa.Column('model_a', sa.VARCHAR(length=120), nullable=False),
    sa.Column('model_b', sa.VARCHAR(length=120), nullable=False),
    sa.Column('model_c', sa.VARCHAR(length=120), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('user_id')
    )
    op.drop_table('useritemmodel')
    # ### end Alembic commands ###
