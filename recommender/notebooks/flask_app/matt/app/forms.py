from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField
from wtforms.validators import DataRequired

class CompareForm(FlaskForm):
    user_id = StringField('User ID', validators=[DataRequired()])
    item_id = StringField('Item ID', validators=[DataRequired()])
    model_a = StringField('Model A', validators=[DataRequired()])
    model_b = StringField('Model B', validators=[DataRequired()])
    model_c = StringField('Model C', validators=[DataRequired()])

class useridForm(FlaskForm):
    user_id=IntegerField('User ID', validators=[DataRequired()])