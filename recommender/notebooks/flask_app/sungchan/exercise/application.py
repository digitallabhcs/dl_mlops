from flask import Flask, render_template, request, url_for, redirect
import pandas as pd
import sys
import database
application = Flask(__name__)


@application.route("/")
def hello():
    # return "Hello goorm!"
    return render_template("hello.html")

# @application.route("/algorithm")
# def algorithm():
#     return render_template("algorithm.html")


@application.route("/apply")
def apply():
    return render_template("apply.html")


@application.route("/applyphoto")
def photo():
    item = request.args.get("item")
    photo_yn = request.args.get("photo_yn")
    description = request.args.get("description")

    if photo_yn == "None":
        photo_yn = False
    else:
        photo_yn = True

    database.save(item, photo_yn, description)
    return render_template("applyphoto.html")


@application.route("/upload_done", methods=["POST"])
def upload_done():
    uploaded_files = request.files["file"]
    uploaded_files.save("static/img/{}.jpg".format(database.now_index()))

    return redirect(url_for("hello"))


@application.route("/product_list")
def product_list():
    proudct_list = database.load_list()
    length = len(proudct_list)
    return render_template("product_list.html", product_list=proudct_list, length=length)


@application.route("/product_info/<int:index>/")
def product_info(index):
    product = database.load_product(index)
    # print(product)
    item = product["item"]
    photo_yn = product["photo_yn"]
    description = product["description"]
    # print(item, photo_yn, description)

    photo = f'img/{index}.jpg'

    return render_template("product_info.html", item=item, photo_yn=photo_yn, description=description, photo=photo)
    # return None


# @application.route("/house_info/<int:index>/")
# def house_info(index):
#     house_info = database.load_house(index)
#     location = house_info["location"]
#     cleaness = house_info["cleaness"]
#     built_in = house_info["built_in"]

#     photo = f"img/{index}.jpeg"

#     return render_template("house_info.html", location=location, cleaness=cleaness, built_in=built_in, photo=photo)

# @application.route("/results")
# def results():
#     # return render_template("reco_resutls.html")
#     return render_template("resutls.html")


if __name__ == "__main__":
    application.run(host='0.0.0.0')
