import pandas as pd


def save(item, photo_yn, description):
    idx = len(pd.read_csv('database.csv'))
    new_df = pd.DataFrame({'item': item,
                           'photo_yn': photo_yn,
                           'description': description
                           }, index=[idx]
                          )
    new_df.to_csv("database.csv", mode='a', header=False)

    return None


def load_list():
    product_list = []
    df = pd.read_csv('database.csv')
    for i in range(len(df)):
        product_list.append(df.iloc[i].to_list())

    print(product_list)


def now_index():
    return len(pd.read_csv('database.csv') - 1)


def load_product(idx):
    df = pd.read_csv('database.csv')

    return df.iloc[idx]


if __name__ == '__main__':
    load_list()
