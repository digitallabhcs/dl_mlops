from flask import Flask, render_template, request
import sys
application = Flask(__name__)


@application.route("/")
def hello():
    # return "Hello goorm!"
    return render_template("hello.html")

# @application.route("/algorithm")
# def algorithm():
#     return render_template("algorithm.html")


@application.route("/apply")
def algorithm():
    return render_template("apply.html")


@application.route("/applyphoto")
def photo():
    item = request.args.get("item")
    photo = request.args.get("photo")
    description = request.args.get("description")
    # return render_template("algorithm.html")


@application.route("/results")
def results():
    # return render_template("reco_resutls.html")
    return render_template("resutls.html")


if __name__ == "__main__":
    application.run(host='0.0.0.0')
