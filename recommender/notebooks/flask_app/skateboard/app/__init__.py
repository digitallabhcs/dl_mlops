from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
import config, os

db = SQLAlchemy()
migrate = Migrate()

BASE_DIR = os.path.dirname(__file__)

def create_app(config):
    app = Flask(__name__)

    app.config.from_object(config)
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///{}'.format(os.path.join(BASE_DIR, 'reco.db'))
    app.config['SECRET_KEY'] = "dev"

    # 
    db.init_app(app)
    migrate.init_app(app, db)

    from . import models

    from .views import main_views, compare_views
    app.register_blueprint(main_views.bp)
    app.register_blueprint(compare_views.bp)

    return app