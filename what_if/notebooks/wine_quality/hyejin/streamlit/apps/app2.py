# app2.py
import streamlit as st
import pandas as pd
import numpy as np
import altair as alt
from interpret.glassbox import ExplainableBoostingRegressor
import charts
import utils
import joblib

data_path = '../../../../data/wine_quality/raw/wine_quality.csv'
model_path = '../../../../data/wine_quality/model/wine_quality_ebm_wip.pkl'

def app():
    st.title('와인 Quality는 11개의 성분으로 설명됩니다')
    st.subheader('모델링 결과는 다음과 같습니다')

    # st.subheader('와인 table')
    df = pd.read_csv(data_path)
    st.write("")
    
    model = joblib.load(model_path)
    
    # st.subheader('와인 Quality 모델 결과')
    
    col1, col2 = st.beta_columns(2)
    
    col1.write('와인 Quality 분포')
    col1.write(charts.y_dist_figure(df,'quality',40,'와인 Quality'))

    col2.write("alcohol ")
    # col2.write()

    col3, col4 = st.beta_columns(2)
    col3.write("Feature importance")
    df_feature_importance = utils.get_feature_importance_df(df.iloc[:,:-1], model)
    col3.write(charts.feature_importance_figure(df_feature_importance, 20))
    
    col4.write("citric acid")
    # col4.write(c)
    