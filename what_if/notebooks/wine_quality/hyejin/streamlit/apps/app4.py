# app2.py
import streamlit as st
import pandas as pd
import numpy as np
import altair as alt

def app():
    wine_id = st.sidebar.text_input("Wine ID: (1~4898)", "7")
    st.title('와인 ID {} : Simulation'.format(wine_id))
    st.subheader('Simulation 하고 싶은 quality range를 입력하세요')
    target_range = st.sidebar.slider("Select the quality range", 0, 10, (4, 5), 1)

    st.subheader('와인 품질을 {}로 바꾸려면 다음과 같은 속성들을 변화시켜야 합니다'.format(target_range))
    df = pd.DataFrame(np.random.randn(200, 3), columns=['a', 'b', 'c'])
    st.table(df.head(2))

    c = alt.Chart(df).mark_circle().encode(
        x='a', y='b', size='c', color='c', tooltip=['a', 'b', 'c'])
    
    col1, col2, col3 = st.beta_columns(3)
    
    col1.subheader('와인 Quality 속성 변화 chart')
    col1.write(c)

    col2.subheader("Chart2")
    col2.write(c)

    col3.subheader("Chart3")
    col3.write(c)
    