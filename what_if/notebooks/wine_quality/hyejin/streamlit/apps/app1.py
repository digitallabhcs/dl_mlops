# app1.py
import streamlit as st
import pandas as pd
import numpy as np
import altair as alt
import joblib

dict_path = '../../../../data/wine_quality/processed/wine_quality_data_dict.pkl'

def app():
    st.title('와인의 품질은 11가지 성분으로 분해됩니다')
    st.subheader('각각의 성분들은 다음과 같습니다')
    
    df = joblib.load(dict_path)
    st.table(df)