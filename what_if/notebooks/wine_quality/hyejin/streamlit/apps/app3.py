# app2.py
import streamlit as st
import pandas as pd
import numpy as np
import altair as alt

def app():
    wine_id = st.sidebar.text_input("Wine ID: (1~4898)", "7")
    st.title('와인 ID {} Quality 이해하기'.format(wine_id))
    
    st.subheader('Wine table')
    df = pd.DataFrame(np.random.randn(200, 3), columns=['a', 'b', 'c'])
    st.table(df.head(2))

    st.subheader('Wine chart')
    c = alt.Chart(df).mark_circle().encode(
        x='a', y='b', size='c', color='c', tooltip=['a', 'b', 'c'])
    
    col1, col2, col3 = st.beta_columns(3)
    
    col1.subheader('fixed acidity 한글로')
    col1.write(c)

    col2.subheader("alcohol ")
    col2.write(c)

    col3.subheader("citric acid")
    col3.write(c)
    
