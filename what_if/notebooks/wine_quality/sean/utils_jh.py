#!/usr/bin/env python
# coding: utf-8

# In[ ]:

import pandas as pd 
import numpy as np

def classification_result(model, x, y):
    '''
    Fuction to create dataframe of classification result.
    
    * params :
    ---------------------------
      model: classifier object (e.g. ebm, lgbm, xgboost, rf, ..) 
      x: dataframe of features
      y: series of target value
    
    
    * returns: 
    ---------------------------
      df_ebm_result (y_score, y_true, y_pred, class(TP/TN/FP/FN), features)
    
    [Usage]
    >> make_df_ebm_result(ebm, X_valid, y_valid)
    >>
	y_proba_0	y_proba_1	y_true	y_pred	class	var_0	var_1	var_2	var_3	var_4
0	0.909503	0.090497	0	0	TN	12.7424	-0.7690	8.3472	7.6773	9.2434
1	0.977724	0.022276	0	0	TN	8.6081	-3.4688	12.9357	4.2776	9.4651
2	0.492512	0.507488	1	1	TP	12.8177	1.6222	7.1529	12.2844	14.6363
    '''
    
    # Prediction Result
    target = pd.DataFrame(data=y).columns.tolist()

    y_score = pd.DataFrame(data=model.predict_proba(x)).rename({0:'y_proba_0', 1:'y_proba_1'}, axis=1)
    y_true = pd.DataFrame(data=y).reset_index(drop=True)
    y_true = y_true.rename({y_true.columns[0]:'y_pred'}, axis=1)
    y_pred = pd.DataFrame(data=model.predict(x)).rename({0:'y_pred'}, axis=1)
    
    df = pd.concat([y_score, y_true, y_pred], axis=1)
    
    # Classification Result
    conditionlist = [
        (df['y_true']==1) & (df['y_pred']==1),
        (df['y_true']==0) & (df['y_pred']==0),
        (df['y_true']==0) & (df['y_pred']==1),
        (df['y_true']==1) & (df['y_pred']==0)]
    
    choicelist = ['TP', 'TN', 'FP', 'FN']
    
    df['class'] = np.select(conditionlist, choicelist, default='Not Specified')
    df_ebm_summary = pd.concat([df, x.reset_index(drop=True)], axis=1)
    
    return df_ebm_summary

