
# https://pycaret.readthedocs.io/en/latest/api/classification.html?highlight=setup#pycaret.classification.setup
from pycaret import classification

classification.setup(data, 
                    target, 
                    train_size, 
                    preprocess(default = True), 
                    imputation_type: str, default = ‘simple’
                    categorical_features: list of str, default = None
                    categorical_imputation: str, default = ‘constant’
                    numeric_imputation: str, default = ‘mean’(median, zero)
                    ignore_features: list of str, default = None
                    normalize: bool, default = False




                    )


pycaret.regression.setup(data: pandas.core.frame.DataFrame, 
                        target: str, 
                        train_size: float = 0.7, 
                        test_data: Optional[pandas.core.frame.DataFrame] = None, 
                        preprocess: bool = True, 
                        imputation_type: str = 'simple', 
                        iterative_imputation_iters: int = 5, 
                        categorical_features: Optional[List[str]] = None, 
                        categorical_imputation: str = 'constant', 
                        categorical_iterative_imputer: Union[str, Any] = 'lightgbm', 
                        ordinal_features: Optional[Dict[str, list]] = None, 
                        high_cardinality_features: Optional[List[str]] = None, 
                        high_cardinality_method: str = 'frequency', 
                        numeric_features: Optional[List[str]] = None, 
                        numeric_imputation: str = 'mean', 
                        numeric_iterative_imputer: Union[str, Any] = 'lightgbm', 
                        date_features: Optional[List[str]] = None, 
                        ignore_features: Optional[List[str]] = None, 
                        normalize: bool = False, 
                        normalize_method: str = 'zscore', 
                        transformation: bool = False, 
                        transformation_method: str = 'yeo-johnson', 
                        handle_unknown_categorical: bool = True, 
                        unknown_categorical_method: str = 'least_frequent', 
                        pca: bool = False, pca_method: str = 'linear', 
                        pca_components: Optional[float] = None, 
                        ignore_low_variance: bool = False, 
                        combine_rare_levels: bool = False, 
                        rare_level_threshold: float = 0.1, 
                        bin_numeric_features: Optional[List[str]] = None, 
                        remove_outliers: bool = False, 
                        outliers_threshold: float = 0.05, 
                        remove_multicollinearity: bool = False, 
                        multicollinearity_threshold: float = 0.9, 
                        remove_perfect_collinearity: bool = True, 
                        create_clusters: bool = False, cluster_iter: int = 20, 
                        polynomial_features: bool = False, 
                        polynomial_degree: int = 2, 
                        trigonometry_features: bool = False, 
                        polynomial_threshold: float = 0.1, 
                        group_features: Optional[List[str]] = None, 
                        group_names: Optional[List[str]] = None, 
                        feature_selection: bool = False, 
                        feature_selection_threshold: float = 0.8, 
                        feature_selection_method: str = 'classic', 
                        feature_interaction: bool = False, 
                        feature_ratio: bool = False, 
                        interaction_threshold: float = 0.01, 
                        transform_target: bool = False, 
                        transform_target_method: str = 'box-cox', 
                        data_split_shuffle: bool = True, 
                        data_split_stratify: Union[bool, List[str]] = False, 
                        fold_strategy: Union[str, Any] = 'kfold', fold: int = 10, 
                        fold_shuffle: bool = False, 
                        fold_groups: Optional[Union[str, pandas.core.frame.DataFrame]] = None, 
                        n_jobs: Optional[int] = - 1, 
                        use_gpu: bool = False, 
                        custom_pipeline: Optional[Union[Any, Tuple[str, Any], List[Any], List[Tuple[str, Any]]]] = None, 
                        html: bool = True, 
                        session_id: Optional[int] = None, 
                        log_experiment: bool = False, 
                        experiment_name: Optional[str] = None, 
                        log_plots: Union[bool, list] = False, 
                        log_profile: bool = False, 
                        log_data: bool = False, 
                        silent: bool = False, 
                        verbose: bool = True, 
                        profile: bool = False, 
                        profile_kwargs: Optional[Dict[str, Any]] = None)