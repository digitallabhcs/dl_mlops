import streamlit as st
from multiapp import MultiApp
from apps import app1, app2 # import your app modules here


app = MultiApp()

# Add all your application here
app.add_app("App1", app1.app)
app.add_app("App2", app2.app)

# The main app
app.run()