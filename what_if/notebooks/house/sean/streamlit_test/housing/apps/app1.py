from sklearn.manifold import TSNE
import matplotlib.pyplot as pl
import streamlit as st
import pandas as pd
import numpy as np
import altair as alt
import altair_viewer
import pickle
import shap


st.set_option('deprecation.showPyplotGlobalUse', False)


@st.cache
def load_df_data():
    df_pickle = '/Users/a405305/PycharmProjects/mlops/data/wine_data/original_df.pickle'
    with open(df_pickle, 'rb') as handle:
        df = pickle.load(handle)
    return df


def load_model_as_shap():
    model_pickle = '/Users/a405305/PycharmProjects/mlops/pycaret/model/tuned_xgboost_saved_0524_1712.pickle'
    with open(model_pickle, 'rb') as handle:
        xgb_saved = pickle.load(handle)
    explainer = shap.TreeExplainer(xgb_saved)
    return explainer


def shap_summary(shap_values, X):
    shap.summary_plot(shap_values, X, plot_type="bar", show=False)
    st.pyplot(bbox_inches='tight')
    pl.clf()


def shap_summary_plot(shap_values, X):
    shap.summary_plot(shap_values, X, show=False)
    st.pyplot(bbox_inches='tight')
    pl.clf()


def TSNE_transform(shap_values, X, df):
    shap_embedded = TSNE(n_components=2, perplexity=25, random_state=34).fit_transform(shap_values)
    source = X.copy()
    source.insert(len(source.columns), "TSNE-1", shap_embedded[:, 0])
    source.insert(len(source.columns), "TSNE-2", shap_embedded[:, 1])
    source.insert(len(source.columns), "TARGET", shap_values.sum(1).astype(np.float64))
    bins = [3, 4, 5, 6, 7, 8]
    labels = ['lowest', 'p4', 'p5', 'p6', 'p7', 'highest']
    source['TARGET_BINNED'] = pd.cut(df['quality'], bins=6, labels=labels).astype(str)
    return source


def app():
    st.title('Wine Quality - SHAP')
    df = load_df_data()
    st.write('Input data description')
    st.write(df.describe())
    explainer = load_model_as_shap()
    X = df.drop('quality', 1)
    shap_values = explainer.shap_values(X)
    expectation = explainer.expected_value
    st.write('와인 퀄리티의 SHAP 평균값은? ', round(expectation, 2))
    st.subheader('shap summary')
    shap_summary(shap_values, X)
    st.subheader('shap summary plot')
    shap_summary_plot(shap_values, X)
    st.subheader('TSNE (shap_values를 가지고 clustering 하기)')
    source = TSNE_transform(shap_values, X, df)
    brush = alt.selection(type='interval', resolve='global')
    hist = alt.Chart(source).mark_bar().encode(
        x=alt.X("TARGET:Q", bin=alt.BinParams(maxbins=30)),
        y='count()',
    )
    points_TSNE = alt.Chart(source.loc[source['TARGET_BINNED'].isin(['p5', 'p6', 'p7'])]).mark_point().encode(
                                x='TSNE-1:Q',
                                y='TSNE-2:Q',
                                color=alt.condition(brush, 'TARGET_BINNED:N', alt.value('lightgray'))
                            ).add_selection(
                                brush
                            ).properties(
                                width=400,
                                height=300
                            )
    points_binned_TSNE = alt.Chart(source.loc[source['TARGET_BINNED'].isin(['lowest', 'p4', 'highest'])]).mark_circle(
                                size=80).encode(
                                x='TSNE-1:Q',
                                y='TSNE-2:Q',
                                color=alt.condition(brush, 'TARGET_BINNED:N', alt.value('lightgray')),
                                tooltip=['TARGET_BINNED']
                            ).add_selection(
                                brush
                            ).properties(
                                width=400,
                                height=300
                            )
    points_alcohol = alt.Chart(source.loc[source['TARGET_BINNED'].isin(['p5', 'p6', 'p7'])]).mark_point().encode(
                                x='alcohol:Q',
                                y='TARGET:Q',
                                color=alt.condition(brush, 'TARGET_BINNED:N', alt.value('lightgray'))
                            ).add_selection(
                                brush
                            ).properties(
                                width=400,
                                height=300
                            )
    st.altair_chart(hist & points_TSNE & points_binned_TSNE & points_alcohol)
