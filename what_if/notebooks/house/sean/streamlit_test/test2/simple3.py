import pandas as pd
import streamlit as st
import altair as alt
import numpy as np
import math


st.set_page_config(page_title='Streamlit Sandbox', layout='wide', initial_sidebar_state='collapsed')
st.sidebar.title("Filter")


@st.cache
def load_dataset(data_link, delimiter=','):
    dataset = pd.read_csv(data_link, delimiter=delimiter)
    return dataset


wine_link = '../data/wine_data/winequality-red.csv'
wine = load_dataset(wine_link, delimiter=';')

# describe_expander
describe_expander = st.beta_expander("View DataFrame Description", expanded=False)
describe_expander.dataframe(wine.describe().T)
describe_expander.dataframe(wine.head(3))

# histogram
histogram_expander = st.beta_expander("View DataFrame Histogram", expanded=False)
with histogram_expander:
    field = st.selectbox(
        "Choose field", list(wine.columns)
    )
    qualities = st.multiselect(
        "Choose quality", sorted(wine['quality'].unique()), [5]
    )
    hist = alt.Chart(wine.loc[wine['quality'].isin(qualities)]).mark_bar().encode(
        x=alt.X(field, bin=alt.BinParams(maxbins=100)),
        y='count()'
    )
    red_median_line = alt.Chart(wine).mark_rule(color='red').encode(
        x=alt.X('mean({})'.format(field)),
        size=alt.value(5)
    )
    histogram_expander.altair_chart(hist + red_median_line)


