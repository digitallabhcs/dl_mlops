import pandas as pd
import time
import streamlit as st
import plotly.express as px
import math

st.set_page_config(page_title='Streamlit Sandbox', layout='wide', initial_sidebar_state='collapsed')
st.sidebar.title("Filter")

@st.cache
def load_dataset(data_link):
    dataset = pd.read_csv(data_link)
    return dataset


titanic_link = 'https://raw.githubusercontent.com/mwaskom/seaborn-data/master/titanic.csv'
titanic_data = load_dataset(titanic_link)

c1, c2, c3 = st.beta_columns((2, 1, 1))

with c1:
    my_expander = st.beta_expander("View DataFrame Description", expanded=False)
    my_expander.dataframe(titanic_data.describe().T)

with c2:
    age_min = st.number_input("Minimum Age", value=math.trunc(titanic_data['age'].min()))

with c3:
    age_max = st.number_input("Maximum Age", value=math.ceil(titanic_data['age'].max()))

if age_max < age_min:
    st.error("The maximum age can't be smaller than the minimum age!")
else:
    st.success("Congratulations! Correct Parameters!")
    subset_age = titanic_data[(titanic_data['age'] <= age_max) & (age_min <= titanic_data['age'])]
    st.write(f"Number of Records With Age Between {age_min} and {age_max}: {subset_age.shape[0]}")
    st.dataframe(subset_age)