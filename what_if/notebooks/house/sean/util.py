import re


def upper_to_underscore(text):
    return re.sub( '(?<!^)(?=[A-Z])', '_', text ).lower()
