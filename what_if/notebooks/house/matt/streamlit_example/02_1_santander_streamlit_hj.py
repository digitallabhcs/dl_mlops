import streamlit as st
import pandas as pd
import numpy as np
import plotly.graph_objects as go
import plotly.express as px
from plotly import tools
import plotly.offline as py
from plotly.subplots import make_subplots
import matplotlib.pyplot as plt
from streamlit_agraph import agraph, Node, Edge, Config
import joblib
import plotly.figure_factory as ff


st.set_page_config(
    page_title="1st App",
    layout="wide",
    initial_sidebar_state="expanded",
)

st.title('What-if : santander transaction prediction')
st.write("ISFJ @ Digital Lab")


customer_list = 'customer_list.pickle'
cust_list = pd.read_pickle(customer_list)
customer_selected = st.sidebar.selectbox("Select a Customer", cust_list)
# st.write(customer_selected)

result_data = ('./01_5_ebm_score_global.pickle')
raw_csv = '../../../../data/santander/raw/train.csv'
raw = pd.read_csv(raw_csv)

@st.cache
def load_data(DATA_URL):
    data = pd.read_pickle(DATA_URL)
    lowercase = lambda x: str(x).lower()
    data.rename(lowercase, axis='columns', inplace=True)
    return data

# df_load_state = st.text('Loading data...')
df = load_data(result_data)
# df_load_state.text("Done! Start what-if simulation!")

st.subheader(customer_selected)

local_df = ('./01_5_ebm_score_local.pickle')
cust_index = int(customer_selected.split()[1])

def load_local_data(DATA_URL, index):
    data = pd.read_pickle(DATA_URL)
    lowercase = lambda x: 'var_'+str(x).lower()
    data.rename(lowercase, axis='columns', inplace=True)
    data = data.iloc[index,:200].sort_values(ascending=False)
    sorted_col = data.index[:50].to_list()
    data = pd.DataFrame(data.iloc[:50]).transpose()
    return data, sorted_col

cust_local, feat_list = load_local_data(local_df, cust_index)
st.table(cust_local.style.format("{:.4}"))


rank_df = joblib.load('predicted_prob_w_rank.pickle')
# intercept = l['specific'][0]['extra']['scores']
intercept = [-3.189393983845777]
cust_rank = rank_df.loc[cust_index, 'PercentRank']
cust_prob = rank_df.loc[cust_index, 1]

mfig = make_subplots(rows=1, cols=2, horizontal_spacing=0.05, vertical_spacing=0.05)
mfig.add_trace(go.Histogram(x=rank_df[1], histnorm='percent', name=cust_index, nbinsx=40, opacity=0.25), row=1, col=1)
# mfig.add_scatter(x=[cust_prob], y=[30], name=customer_selected, 
#                         mode='markers', marker_size=[5], marker_color=['#ff7f0e'], row=1, col=1)
mfig.add_scatter(x=[cust_prob,cust_prob], y=[0,42], mode='lines', name=customer_selected, row=1, col=1)
mfig.update_xaxes(title_text='Predicted probability and rank of Customer {}'.format(cust_index), row=1, col=1)
mfig.update_layout(barmode='overlay')
mfig.update_layout(height=400, width=1700, showlegend=False)
mfig.update_xaxes(title = "Contribution", row=1, col=2)
mfig.add_trace(go.Waterfall(
    name = customer_selected,
    measure = ["absolute", "relative", "relative", "relative", "relative", "relative", "relative",
              "relative", "relative", "relative", "relative", "relative", "relative",
              "relative", "relative", "relative", "relative", "relative", "relative",
              "relative", "relative", "relative", "relative", "relative", "relative",
              "relative", "relative", "relative", "relative", "relative", "relative",],
    x = ['Intercept'] + feat_list[:30],
    y = list(intercept) + list(cust_local[:30])
),row=1,col=2)
mfig.add_shape(go.layout.Shape(type="line", yref="paper", xref="x",
                                        x0=[cust_prob], y0=0, x1=[cust_prob], y1=40,
                                        line=dict(color="RoyalBlue", dash='dash', width=2),),row=1,col=1)
st.plotly_chart(mfig)

# final_model_path = 'xgb_total_final.pkl'
# model = joblib.load(final_model_path)
# d = pd.DataFrame(model.get_fscore().items(), columns=['feature','importance']).sort_values('importance', ascending=False)

# d = pd.read_csv(model_FI_URL)
# fig = px.bar(d.sort_values('importance',ascending=True), y='feature', x='importance', title='xgboost model', orientation='h')
# fig.update_yaxes(tickfont=dict(size=9))
# fig.update_xaxes(tickfont=dict(size=9))
# fig.update_layout(height=800, width=1200)

# finaldata = load_data(submit_file_URL, 100)
# if st.checkbox('Global scores'):

num_rows = 3
num_cols = 4
fig = make_subplots(specs=[[{"secondary_y": True},{"secondary_y": True},{"secondary_y": True},{"secondary_y": True}],
                           [{"secondary_y": True},{"secondary_y": True},{"secondary_y": True},{"secondary_y": True}],
                           [{"secondary_y": True},{"secondary_y": True},{"secondary_y": True},{"secondary_y": True}]], 
                    rows=num_rows, cols=num_cols, horizontal_spacing=0.05, vertical_spacing=0.05)
# for i in range(len(feat_list)):
# feat_list = df.col_name.drop_duplicates().tolist()
min_score = min(df.scores)
max_score = max(df.scores)

k=0
for i in range(0,num_rows):
    for j in range(0,num_cols):
        x = df[df.col_name == feat_list[k]].names
        y = df[df.col_name == feat_list[k]].scores
        dist = raw.loc[:,feat_list[k]]      
        fig.add_trace(go.Histogram(x=dist,name=feat_list[k], nbinsx=15, opacity=0.25), 
                      secondary_y=True, row=i+1, col=j+1)
        fig.add_scatter(x=x, y=y, name=feat_list[k], mode='lines', secondary_y=False, row=i+1, col=j+1)
        fig.add_scatter(x=[dist[cust_index]], y=[cust_local[feat_list[k]]], name='Customer 0', 
                        mode='markers', marker_size=[15], marker_color=['#ff7f0e'], row=i+1, col=j+1)
        fig.update_xaxes(title_text=feat_list[k], row=i+1, col=j+1)
        fig.update_yaxes(range=[min_score, max_score], secondary_y=False, row=i+1, col=j+1)
        fig.update_yaxes(secondary_y=True,showgrid=False)
        fig.update_layout(barmode='stack',bargap=0.1)
        k+=1

fig.update_layout(height=1100, width=1800, showlegend=False)
# fig.show()
st.plotly_chart(fig)
# if st.checkbox('Plot'):
#     st.subheader('Check the inference')
#     fig = px.scatter(df, 
#         x="cf",
#         y="pred_prob",
#         color="target",
#     )
#     fig.update_layout(
#         xaxis_title="cf",
#         yaxis_title="inference",
#     )
#     st.write(fig)
# if st.checkbox('Submission file'):
#     st.subheader('Final submission')
#     st.write(finaldata)


# # Create row, column, and value inputs
# row = st.number_input('feature', max_value=df.shape[0])
# col = st.number_input('instance', max_value=df.shape[1])
# value = st.number_input('value')

# # Change the entry at (row, col) to the given value
# df.values[row][col] = value

# # And display the result!
# st.dataframe(df)