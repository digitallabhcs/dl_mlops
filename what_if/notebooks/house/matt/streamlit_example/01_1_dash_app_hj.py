import pandas as pd
import joblib
import plotly.graph_objects as go
import plotly.express as px
from plotly import tools
import plotly.offline as py
from plotly.subplots import make_subplots

local_df = ('../scores/01_5_ebm_score_local.pickle')
df = pd.read_pickle('../scores/01_5_ebm_score_global.pickle')
# cust_list = pd.read_pickle('customer_list.pickle')
raw = pd.read_csv('../../../../data/santander/raw/train.csv')

num_rows = 3
num_cols = 4
fig = make_subplots(specs=[[{"secondary_y": True},{"secondary_y": True},{"secondary_y": True},{"secondary_y": True}],
                           [{"secondary_y": True},{"secondary_y": True},{"secondary_y": True},{"secondary_y": True}],
                           [{"secondary_y": True},{"secondary_y": True},{"secondary_y": True},{"secondary_y": True}]], 
                    rows=num_rows, cols=num_cols, horizontal_spacing=0.05, vertical_spacing=0.05)
# for i in range(len(feat_list)):
# feat_list = df.col_name.drop_duplicates().tolist()
min_score = min(df.scores)
max_score = max(df.scores)
def load_local_data(DATA_URL, index):
    data = pd.read_pickle(DATA_URL)
    lowercase = lambda x: 'var_'+str(x).lower()
    data.rename(lowercase, axis='columns', inplace=True)
    data = data.iloc[index,:200].sort_values(ascending=False)
    sorted_col = data.index[:50].to_list()
    data = pd.DataFrame(data.iloc[:50]).transpose()
    return data, sorted_col
# cust_local, feat_list = load_local_data(local_df, int(dropdown_value.split()[1]))
cust_local, feat_list = load_local_data(local_df, 0)

k=0
for i in range(0,num_rows):
    for j in range(0,num_cols):
        x = df[df.col_name == feat_list[k]].names
        y = df[df.col_name == feat_list[k]].scores
        dist = raw.loc[:,feat_list[k]]      
        fig.add_trace(go.Histogram(x=dist,name=feat_list[k], histnorm='percent', nbinsx=15, opacity=0.25), 
                      secondary_y=True, row=i+1, col=j+1)
        fig.add_scatter(x=x, y=y, name=feat_list[k], mode='lines', secondary_y=False, row=i+1, col=j+1)
        fig.add_scatter(x=[dist[0]], y=[cust_local[feat_list[k]]], name='Customer 0', 
                        mode='markers', marker_size=[15], marker_color=['#ff7f0e'], secondary_y=False, row=i+1, col=j+1)
        fig.update_xaxes(title_text=feat_list[k], row=i+1, col=j+1)
        fig.update_yaxes(range=[min_score, max_score], secondary_y=False, row=i+1, col=j+1)
        fig.update_yaxes(secondary_y=True,showgrid=False)
        fig.update_layout(barmode='overlay',bargap=0.1)
        k+=1
fig.update_layout(height=1100, width=1800, showlegend=False)


import dash
import dash_core_components as dcc
import dash_html_components as html

app = dash.Dash()

app.layout = html.Div(children=[
    html.H4('Customer 360 (what-if)'),
#     dcc.Dropdown(id='dropdown',
#         options=[
#             {'label': i, 'value': i} for i in cust_list
#         ], placeholder='Filter by customer ...'),
#     html.Div(id='table-container')
    dcc.Graph(figure=fig)
])

# @app.callback(
#     dash.dependencies.Output('table-container', 'children'),
#     [dash.dependencies.Input('dropdown', 'value')]
# )

   
app.run_server(debug=True)
    
    
    
# def display_table(dropdown_value):
#     if dropdown_value is None:
#         return generate_table(local_df)

#     dff = df[df.state.str.contains('|'.join(dropdown_value))]
#     return generate_table(dff)

# def generate_table(dataframe, max_rows=10):
#     return html.Table([
#         html.Thead(
#             html.Tr([html.Th(col) for col in dataframe.columns])
#         ),
#         html.Tbody([
#             html.Tr([
#                 html.Td(dataframe.iloc[i][col]) for col in dataframe.columns
#             ]) for i in range(min(len(dataframe), max_rows))
#         ])
#     ])


# external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

# app = dash.Dash(__name__, external_stylesheets=external_stylesheets)


    