import streamlit as st
# from multiapp import MultiApp
# from apps import app1, app2

import sys
sys.path.append("../../../../utils/")

import utils


# app = MultiApp()

# # Add all your application here
# app.add_app("Home", app1.app)
# app.add_app("Data Stats", app2.app)

# The main app


from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_log_error
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler, PolynomialFeatures
from sklearn.impute import SimpleImputer
from sklearn.model_selection import cross_val_score, cross_val_predict
from scipy.stats import norm
import matplotlib.pyplot as plt
# %matplotlib inline
import numpy as np
import pandas as pd
import seaborn as sns
import lightgbm as lgb
import joblib
import math
import os
import glob

print(os.listdir("../../../../data/house"))
print(glob.glob('*.pkl'))

# Load data

filename = '../../../../data/house/ames_shallow_fe.pickle'
df = joblib.load(filename)
df.head(3)

# EBM 은  NA를 용납하지 않는다...
df = df.dropna()
df.shape

# EBM modeling

# interpretml EBM
from interpret.glassbox import ExplainableBoostingRegressor, LinearRegression, RegressionTree
from interpret.data import Marginal
from interpret import show

X = df.drop(['p_i_d','price','log_price'], axis=1)
X.head(3)

y = df['price']

ebm = ExplainableBoostingRegressor(random_state=42)
ebm.fit(X, y)


global_df=utils.get_global_df(X, ebm)


import plotly.graph_objects as go
from plotly.subplots import make_subplots

# scores_range=(-98365.92454651206, 102145.65757499124)
layout_setting = {'xaxis_title':'기간','yaxis_title':'제주 이용객 수','font':dict(size=18,color='#60606e',family='Franklin Gothic' )}

df=global_df.copy()

features = df.col_name.unique()[:10]

fig = make_subplots(rows=10, cols=3, horizontal_spacing=0.1,
                    vertical_spacing= 0.05,
                   subplot_titles=([f'<b>{feat}</b>' for feat in features]))

for i, feat in enumerate(features):
    dff=df[df.col_name==feat]
    row, col, legend = i//3 + 1, i%3 + 1, False
    if i == len(features)-1 :
        legend = True
    fig.add_trace(go.Scatter(x=dff.names, y=dff['scores'],
                     mode='lines', name="Confirmed", line_shape="vh", line=dict(width=2),
                     marker=dict(color = '#6775D0'), showlegend=legend), row=row, col=col)
    fig.add_trace(go.Scatter(x=dff.names, y=dff['upper_bounds'],
                     mode='lines', name="upper_bounds", line_shape="vh", line=dict(width=0.75),
                     marker=dict(color = '#B0B0B0'), showlegend=legend), row=row, col=col)
    fig.add_trace(go.Scatter(x=dff.names, y=dff['lower_bounds'],
                     mode='lines', name="lower_bounds",line_shape="vh", line=dict(width=0.75),
                     marker=dict(color = '#B0B0B0'), showlegend=legend), row=row, col=col)
    font=dict(
        family="Courier New, monospace",
        size=18,
        color="#7f7f7f"
    )
fig.update_layout(title='<b>feature contribution scores</b>',
                  height = 2000, font=dict(size=10),
                  legend=dict(x=0.7, y=0.05, traceorder="normal",
                             font=dict(family="sans-serif", size=10)),
#                  paper_bgcolor='rgba(0,0,0,0)',
                  plot_bgcolor='rgba(0,0,0,0)')

for i in fig['layout']['annotations']:
    i['font'] = dict(size=10)


st.plotly_chart(fig)

