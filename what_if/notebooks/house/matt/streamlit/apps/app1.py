# app1.py
import streamlit as st

# Load and apply

from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_log_error
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler, PolynomialFeatures
from sklearn.impute import SimpleImputer
from sklearn.model_selection import cross_val_score, cross_val_predict
from scipy.stats import norm
import matplotlib.pyplot as plt
# %matplotlib inline
import numpy as np
import pandas as pd
import seaborn as sns
import lightgbm as lgb
import joblib
import math
import os
import glob

print(os.listdir("../../../../data/house"))
print(glob.glob('*.pkl'))

# Load data

filename = '../../../../data/house/ames_shallow_fe.pickle'
df = joblib.load(filename)
df.head(3)

# EBM 은  NA를 용납하지 않는다...
df = df.dropna()
df.shape

# EBM modeling

# interpretml EBM
from interpret.glassbox import ExplainableBoostingRegressor, LinearRegression, RegressionTree
from interpret.data import Marginal
from interpret import show

X = df.drop(['p_i_d','price','log_price'], axis=1)
X.head(3)

y = df['price']

X=X.select_dtypes(exclude="object")

ebm = ExplainableBoostingRegressor(random_state=42, max_rounds=1)
ebm.fit(X, y)

import sys
sys.path.append("../../../../utils/")

import utils

global_df=utils.get_global_df(X, ebm)

score_df=utils.get_score_variance_table(global_df, ebm)

fi_df=utils.get_feature_importance_df(X, ebm)

local_df=utils.get_local_df(X.iloc[:10],y.iloc[:10], ebm)





import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots


def plot_fig_score_plots():


    df=global_df.copy()

    features = df.col_name.unique()[:9]

    fig_score_plots = make_subplots(rows=3, cols=3, horizontal_spacing=0.1,
                        vertical_spacing= 0.05,
                       subplot_titles=([f'<b>{feat}</b>' for feat in features]))

    for i, feat in enumerate(features):
        dff=df[df.col_name==feat]
        row, col, legend = i//3 + 1, i%3 + 1, False
        if i == len(features)-1 :
            legend = False
        fig_score_plots.add_trace(go.Scatter(x=dff.names, y=dff['scores'],
                         mode='lines', name="scores", line_shape="vh", line=dict(width=2),
                         marker=dict(color = '#6775D0'), showlegend=legend), row=row, col=col)
        fig_score_plots.add_trace(go.Scatter(x=dff.names, y=dff['upper_bounds'],
                         mode='lines', name="upper_bounds", line_shape="vh", line=dict(width=0.75),
                         marker=dict(color = '#B0B0B0'), showlegend=legend), row=row, col=col)
        fig_score_plots.add_trace(go.Scatter(x=dff.names, y=dff['lower_bounds'],
                         mode='lines', name="lower_bounds",line_shape="vh", line=dict(width=0.75),
                         marker=dict(color = '#B0B0B0'), showlegend=legend), row=row, col=col)
        font=dict(
            family="Courier New, monospace",
            size=18,
            color="#7f7f7f"
        )
    fig_score_plots.update_layout(title='<b>feature contribution scores</b>',
                      height = 750, font=dict(size=10),
                      legend=dict(x=0.7, y=0.05, traceorder="normal",
                                 font=dict(family="sans-serif", size=10)),
    #                  paper_bgcolor='rgba(0,0,0,0)',
                      plot_bgcolor='rgba(0,0,0,0)')

    for i in fig_score_plots['layout']['annotations']:
        i['font'] = dict(size=10)
    
    return fig_score_plots





#### price histogram

def plot_fig_target_var():
    fig_target_var = px.histogram(y, x="price")
    
    return fig_target_var



#### dataset description


def get_describe_table():
    X_describe=X.describe()
    return X_describe





#### Feature Importance
def plot_fig_feature_importance():
    fi = score_df['feature_importance'].sort_values(ascending=True).T
    fig_feature_importance = px.bar(fi, x='feature_importance', y=fi.index)
    return fig_feature_importance



#### contribution plots
import plotly.graph_objects as go
    
def plot_fig_contribution_plot():

    vis_array=local_df[local_df.val_name=="scores"].drop(["val_name","predicted_score","actual_score","intercept_score"],axis=1).iloc[1]

    vis_df=pd.DataFrame(vis_array)

    vis_df.columns=["contribution"]

    vis_df=vis_df.sort_values("contribution",ascending=False)


    # intercept=local_internal_obj["specific"][0]["extra"]["scores"][0]

    abs_top_k_colnames=vis_df.apply(abs).sort_values("contribution").tail(40).index

    vis_df_top=vis_df[vis_df.index.isin(abs_top_k_colnames)]


    fig_contribution_plot = go.Figure(go.Waterfall(
    #    base=intercept,
        name = "contribution", orientation = "v",
    #     measure = ["relative", "relative", "total", "relative", "relative", "total"],
        x = vis_df_top.index,
        textposition = "outside",
        text = vis_df_top.contribution,
        y = vis_df_top.contribution,
        connector = {"line":{"color":"rgb(63, 63, 63)"}},
    ))

    fig_contribution_plot.update_layout(
            title = "contribution plot",
            showlegend = True
    )

    return fig_contribution_plot



#### expected additional profit plot

def plot_fig_marginal_profit():
    fi= score_df['feature_importance'].sort_values(ascending=True).T
    top_3_fi_colnames=fi[-3:].sort_values(ascending=False).index.tolist()

#    top_3_fi_colnames

    X_marginal_profit=X.copy()

    X_marginal_profit.overall_qual=X_marginal_profit.overall_qual.apply(lambda x: x+1)

    result=ebm.predict(X_marginal_profit)

    marginal_profit_01=(result-y).mean()



    X_marginal_profit=X.copy()

    X_marginal_profit.area=X_marginal_profit.area.apply(lambda x: x+200)

    result=ebm.predict(X_marginal_profit)

    marginal_profit_02=(result-y).mean()



    X_marginal_profit=X.copy()

    X_marginal_profit.total_s_f=X_marginal_profit.total_s_f.apply(lambda x: x+200)

    result=ebm.predict(X_marginal_profit)

    marginal_profit_03=(result-y).mean()



    marginal_profit_df=pd.DataFrame([marginal_profit_01,marginal_profit_02,marginal_profit_03],index=top_3_fi_colnames)

    marginal_profit_df.columns=["marginal_profit"]

    fig_marginal_profit = px.bar(marginal_profit_df, x=marginal_profit_df.index, y="marginal_profit")
    return fig_marginal_profit


#### counterfactual plot


def plot_fig_counter_factual():
    X_part=X.sample(100, random_state=77)

    fig_counter_factual = go.FigureWidget([go.Scatter(x=X_part["area"], y=X_part["lot_area"], mode='markers')])

    scatter = fig_counter_factual.data[0]
    colors = ['#0900FF'] * 100
    scatter.marker.color = colors
    scatter.marker.size = [10] * 100
    fig_counter_factual.layout.hovermode = 'closest'

    # create our callback function
    def update_point(trace, points, selector):
        c = list(scatter.marker.color)
        s = list(scatter.marker.size)
        print(points.point_inds)
        CF_target_index=get_CF_point(points.point_inds)

        CF_idx=X_part.index.get_loc(X_part.loc[CF_target_index].name)

        for i in points.point_inds:
            c[i] = '#00EFFF'
            s[i] = 20

            c[CF_idx] = "#C924CE"
            s[CF_idx] = 20

            with fig_counter_factual.batch_update():
                scatter.marker.color = c
                scatter.marker.size = s

            # change counterfactural point

            with fig_counter_factual.batch_update():
                scatter.marker.color = c
                scatter.marker.size = s
                
                
        scatter.on_click(update_point)

    def get_CF_point(point_idx):
        tar_point_idx=X_part.iloc[point_idx[0]].name
        print(tar_point_idx)
        tar=X_part.loc[tar_point_idx].to_numpy().reshape(1,-1)
        tar_price=y[tar_point_idx]+10000

        dist_dict={}
        for idx in X_part.index:
            if (idx != tar_point_idx) & (y[idx]>=tar_price):
                tmp_array=X_part.loc[idx].to_numpy().reshape(1,-1)
                dist_dict[idx]=manhattan_distances(tar, tmp_array)


        sorted_dist_dict_keys=sorted(dist_dict, key=dist_dict.get, reverse=True)

        sorted_dist_dict_vals=sorted(dist_dict.values(), reverse=True)
        sorted_dist_dict_vals=[val[0][0] for val in sorted_dist_dict_vals]

        counterfactual_candidates=dict(zip(sorted_dist_dict_keys, sorted_dist_dict_vals))

        price_CF_cand=y[counterfactual_candidates.keys()]

        price_CF_cand=price_CF_cand[price_CF_cand<tar_price+100000]

        CF_target_index=price_CF_cand.index[-1]

        return CF_target_index
    
    return fig_counter_factual






#### cumulative marginal profit plot


def plot_fig_cumulative_marginal_profit():
    tar_sample=X.loc[0]

    cumulative_marginal_profit_list=[]
    for i in range(3):
        tar_sample.area=tar_sample.area+1000
        cumulative_marginal_profit_list.append(ebm.predict(pd.DataFrame(tar_sample).T))

    fig_cumulative_marginal_profit = px.bar(cumulative_marginal_profit_list)
    return fig_cumulative_marginal_profit





def app():
    st.markdown("<h1 style='text-align: center; color: black;'>[Model Briefing]</h1>", unsafe_allow_html=True)
    c1, c2 = st.beta_columns([2, 1])

    with c1:
        fig_target_var=plot_fig_target_var()
        st.plotly_chart(fig_target_var)
        
    with c2:
#         st.text_area("Model Briefing", value='Model name: EBM \n RMSE: 20,861 \n haha', height=None, max_chars=None, key=None)
        model_breafing_table=pd.DataFrame(["EBM", 20861, 180135],columns=["Description"], index=["Model name","RMSE", "Intercept"])
        st.table(model_breafing_table)
    

    
    describe_table=get_describe_table()
    st.table(describe_table)
    
#    st.title("[Feature Importance]")
    st.markdown("<h1 style='text-align: center; color: black;'>[Feature Importance]</h1>", unsafe_allow_html=True)

    
    c3, c4 = st.beta_columns([1, 1])

    with c3:
        fig_feature_importance=plot_fig_feature_importance()
        st.plotly_chart(fig_feature_importance)
        
    with c4:
        fig_marginal_profit=plot_fig_marginal_profit()
        st.plotly_chart(fig_marginal_profit)
    
    

    
    