import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
# %matplotlib inline
import seaborn as sns
from scipy.stats import norm
import lightgbm as lgb
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_log_error
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler, PolynomialFeatures
## sklearn.preprocessing.Imputer -> sklearn.impute.SimpleImputer
from sklearn.impute import SimpleImputer
from sklearn.model_selection import cross_val_score, cross_val_predict
import os


os.chdir('/Users/a420777/dl_mlops/what_if/notebooks/house/matt')

### Load housing data

df_train = pd.read_csv("../../../data/raw/house/train.csv")
df_test = pd.read_csv("../../../data/raw/house/test.csv")

## convert numeric to string
df_train["MSSubClass"] = df_train["MSSubClass"].apply(str)
df_train['YrSold'] = df_train['YrSold'].apply(str)
df_train['MoSold'] = df_train['MoSold'].apply(str)

### Missing Data

df_na = (df_train.isnull().sum() / len(df_train)) * 100
df_na = df_na.drop(df_na[df_na == 0].index).sort_values(ascending = False)
missing_data = pd.DataFrame({"Missing Ratio": df_na})

df_na_cols = pd.DataFrame(df_train.dtypes)
missing_data = missing_data.join(df_na_cols, how = 'left')

# missing_data


## Target analysis
### Distribution of target is skewed -> Log transform

### Fill missing value

## Fill with the supplied value
def fill_missing(df, cols, val):
    for col in cols:
        df[col] = df[col].fillna(val)
        
## Fill with the mode
def fill_missing_with_mode(df, cols):
    for col in cols:
        df[col] = df[col].fillna(df[col].mode()[0])
        
## log transform for columns
def add_log(res, cols):
    m = res.shape[1]
    for col in cols:
        res = res.assign(newcol = pd.Series(np.log(1.01 + res[col])).values)
        res.columns.values[m] = col + '_log'
        m +=1
    
    return res



cat_null_cols = ["PoolQC", "MiscFeature", "Alley", "Fence", "FireplaceQu","GarageType", "GarageFinish", "GarageQual", "GarageCond",
                 'BsmtQual', 'BsmtCond', 'BsmtExposure', 'BsmtFinType1', 'BsmtFinType2',
                "MasVnrType", "MSSubClass", "Electrical"]

num_null_cols = ['GarageYrBlt', 'MasVnrArea']

fill_missing(df_train, cat_null_cols, "None")
fill_missing(df_train, num_null_cols, 0)

# house co located are similar in size
df_train['LotFrontage'] = df_train.groupby("Neighborhood")["LotFrontage"].transform(lambda x: x.fillna(x.median()))

### Add new feature

## total square feet
df_train["TotalSF"] = df_train['TotalBsmtSF'] + df_train['1stFlrSF'] + df_train['2ndFlrSF']

## Modeling(EBM)

# interpretml EBM
from interpret import show
from interpret.data import Marginal
from interpret.glassbox import ExplainableBoostingRegressor, LinearRegression, RegressionTree

X = df_train.drop(['Id', 'SalePrice'], axis= 1)
y = df_train.SalePrice

X_train, X_valid, y_train, y_valid = train_test_split(X, y, test_size = 0.2, random_state= 42)

marginal = Marginal().explain_data(X_train, y_train, name = 'Train Data')
# show(marginal)

# Explainable boosting regressor
ebm = ExplainableBoostingRegressor(random_state= 42)
ebm.fit(X_train, y_train)


 #performance
from interpret.perf import RegressionPerf
ebm_perf = RegressionPerf(ebm.predict).explain_perf(X_valid, y_valid, name = 'EBM')
# show(ebm_perf)

# ebm globa
ebm_global = ebm.explain_global(name = 'EBM')
# show(ebm_global)


def get_global_df(ebm_global, feature_names):
    global_internal_obj=ebm_global._internal_obj
    global_info_list=[]

    global_data=global_internal_obj["specific"]

    for i in range(len(feature_names)):
        
        col_name=feature_names[i]

        tar=global_data[i]
        
        if "scores_range" in tar:
            scores_range=tar.pop("scores_range")
        if "density" in tar:
            density=tar.pop("density")
        
        if len(tar['names'])!=len(tar['scores']):
            tar["names"]=tar["names"][:-1]
        
        tar=pd.DataFrame(tar)
        tar["col_name"]=col_name
        
        global_info_list.append(tar)

    return pd.concat(global_info_list,axis=0)

global_df=get_global_df(ebm_global, X_train.columns)


def get_score_variance_table(global_df):
    min_max_series=global_df.groupby("col_name").apply(lambda x: x.scores.max()-x.scores.min())
    df=pd.DataFrame(min_max_series, columns={"max_min_gap"})

    FI_df=pd.DataFrame(list(zip(ebm.feature_names, ebm.feature_importances_)))
    FI_df=FI_df.set_index(0)

    df=df.merge(FI_df, right_index=True, left_index=True, how="left")
    df.columns=["max_min_gap","feature_importance"]

    df["std"]=global_df.groupby("col_name").apply(lambda x: x.scores.std())
    return df



get_score_variance_table(global_df)


global_df
min_max_series=global_df.groupby("col_name").apply(lambda x: x.scores.max()-x.scores.min())
df=pd.DataFrame(min_max_series, columns={"max_min_gap"})

FI_df=pd.DataFrame(list(zip(ebm.feature_names, ebm.feature_importances_)))
FI_df=FI_df.set_index(0)

df=df.merge(FI_df, right_index=True, left_index=True, how="left")
df.columns=["max_min_gap","feature_importance"]

df["std"]=global_df.groupby("col_name").apply(lambda x: x.scores.std())

df
df



def append_prediction(df, model):
    df["prediction"]=model.predict(df)
    return df

X_valid_cp=X_valid.copy()
appended_df=append_prediction(X_valid_cp, ebm)







import copy

class EBMAnalysis:
    def __init__(self, ebm_model):
        self.ebm_model=ebm_model
        self.ebm_global = ebm_model.explain_global(name = 'EBM')
        #  self.feature_names=ebm_model.feature_names
        self.feature_names=X_train.columns.tolist()

        self.global_internal_obj=copy.deepcopy(self.ebm_global._internal_obj)
        self.global_df=self.get_global_df(self.global_internal_obj)


    def get_global_df(self, global_internal_obj):

        global_info_list=[]

        global_data=self.global_internal_obj["specific"]

        for i in range(len(self.feature_names)):
            
            col_name=self.feature_names[i]

            tar=global_data[i]
            
            if "scores_range" in tar:
                scores_range=tar.pop("scores_range")
            if "density" in tar:
                density=tar.pop("density")
            
            if len(tar['names'])!=len(tar['scores']):
                tar["names"]=tar["names"][:-1]
            
            tar=pd.DataFrame(tar)
            tar["col_name"]=col_name
            
            global_info_list.append(tar)

        return pd.concat(global_info_list,axis=0)

    def get_score_variance_table(self):
        min_max_series=self.global_df.groupby("col_name").apply(lambda x: x.scores.max()-x.scores.min())
        df=pd.DataFrame(min_max_series, columns={"max_min_gap"})

        FI_df=pd.DataFrame(list(zip(ebm.feature_names, ebm.feature_importances_)))
        FI_df=FI_df.set_index(0)

        df=df.merge(FI_df, right_index=True, left_index=True, how="left")
        df.columns=["max_min_gap","feature_importance"]

        df["std"]=self.global_df.groupby("col_name").apply(lambda x: x.scores.std())
        self.score_variance_table=df
        return self.score_variance_table







test=EBMAnalysis(ebm)

table=test.get_score_variance_table()


