import streamlit as st
import pandas as pd
import numpy as np
import plotly.graph_objects as go
import plotly.express as px
from plotly import tools
import plotly.offline as py
from plotly.subplots import make_subplots
import plotly.figure_factory as ff
import matplotlib.pyplot as plt
import joblib
from functools import reduce
from itertools import product
from sklearn.metrics import roc_curve, roc_auc_score, classification_report
# import utils

st.set_page_config(
    page_title="1st App",
    layout="wide",
    initial_sidebar_state="expanded",
)
st.title('What-if : Ames housing price')
st.write("@ Digital Lab")

st.sidebar.title("Choose your scope")
selectbox = st.sidebar.radio("", ("Global", "Local"))

# data path
raw_csv = '../../../../data/house/ames_shallow_fe.pickle'
