import streamlit as st
import pandas as pd
import numpy as np
import plotly.graph_objects as go
import plotly.express as px
import matplotlib.pyplot as plt
from streamlit_agraph import agraph, Node, Edge, Config
import joblib

st.set_page_config(
    page_title="1st App: What-if for santander transaction prediction",
    layout="wide",
    initial_sidebar_state="expanded",
)

st.title('Dashboard by ISFJ')
st.write("Let's make a minimum-viable-product")

result_data = ('../01_5_result_ebm_raw_streamlit_hj.pickle')

@st.cache
def load_data(DATA_URL):
    data = pd.read_pickle(DATA_URL)
    lowercase = lambda x: str(x).lower()
    data.rename(lowercase, axis='columns', inplace=True)
    return data

df_load_state = st.text('Loading data...')
df = load_data(result_data)
df_load_state.text("Done! (using st.cache)")


# final_model_path = 'xgb_total_final.pkl'
# model = joblib.load(final_model_path)
# d = pd.DataFrame(model.get_fscore().items(), columns=['feature','importance']).sort_values('importance', ascending=False)

# d = pd.read_csv(model_FI_URL)
# fig = px.bar(d.sort_values('importance',ascending=True), y='feature', x='importance', title='xgboost model', orientation='h')
# fig.update_yaxes(tickfont=dict(size=9))
# fig.update_xaxes(tickfont=dict(size=9))
# fig.update_layout(height=800, width=1200)

# finaldata = load_data(submit_file_URL, 100)

if st.checkbox('Table'):
    st.subheader('Check the model results')
    st.write(df)
    
if st.checkbox('Plot'):
    st.subheader('Check the inference')
    fig = px.scatter(df, 
        x="cf",
        y="pred_prob",
        color="target",
    )
    fig.update_layout(
        xaxis_title="cf",
        yaxis_title="inference",
    )
    st.write(fig)
# if st.checkbox('Submission file'):
#     st.subheader('Final submission')
#     st.write(finaldata)

    

# add_selectbox = st.sidebar.selectbox("Select Box", ("EDA", "Modeling result", "Hyperparemter tuning"))
    
# st.subheader('Number of pickups by hour')
# hist_values = np.histogram(data[DATE_COLUMN].dt.hour, bins=24, range=(0,24))[0]
# st.bar_chart(hist_values)

# hour_to_filter = st.slider('hour', 0, 23, 17)
# filtered_data = data[data[DATE_COLUMN].dt.hour == hour_to_filter]

# st.subheader('Map of all pickups at %s:00' % hour_to_filter)
# st.map(filtered_data)