import streamlit as st
import pandas as pd
import numpy as np
import plotly.graph_objects as go
import plotly.express as px
from plotly import tools
import plotly.offline as py
from plotly.subplots import make_subplots
import matplotlib.pyplot as plt
import joblib

import streamlit as st

st.set_page_config(
    page_title="1st App",
    layout="wide",
    initial_sidebar_state="expanded",
)

st.title('What-if : santander transaction prediction')
st.write("@ Digital Lab")

# select a customer at the left sidebar
customer_list = 'customer_list.pickle'
cust_list = pd.read_pickle(customer_list)
customer_selected = st.sidebar.selectbox("Select a Customer", cust_list)
st.subheader(customer_selected)
cust_index = int(customer_selected.split()[1])


# load for charting
# global score
result_data = ('../scores/02_1_ebm_score_global.pickle')

# raw data for histogram
raw_csv = '../../../../data/santander/raw/train.csv'
raw = pd.read_csv(raw_csv)

@st.cache
def load_data(DATA_URL):
    data = pd.read_pickle(DATA_URL)
    lowercase = lambda x: str(x).lower()
    data.rename(lowercase, axis='columns', inplace=True)
    return data
df = load_data(result_data)

# data for waterfall chart and local value/score table
def load_local_data(DATA_URL, index):
    data = pd.read_pickle(DATA_URL)
    lowercase = lambda x: 'var_'+str(x).lower()
    data.rename(lowercase, axis='columns', inplace=True)

    # sort by absolute score 
    data = data.iloc[index,:200].sort_values(key=abs,ascending=False)
    sorted_col = data.index[:40].to_list()
    data = pd.DataFrame(data.iloc[:40]).transpose()
    return data, sorted_col

local_df = ('../scores/01_5_ebm_score_local.pickle')
cust_local, feat_list = load_local_data(local_df, cust_index)
# table formatting
cust_table = pd.DataFrame(raw.loc[cust_index, feat_list]).transpose()
st.table(cust_table.style.format("{:.3}"))

# intercept value 
# intercept = l['specific'][0]['extra']['scores']
intercept = [-3.189393983845777]

rank_df = joblib.load('predicted_prob_w_rank.pickle')
cust_prob = rank_df.loc[cust_index, 1]
# cust_rank = rank_df.loc[cust_index, 'PercentRank']

# predicted prob and contribution 
def upper_figure():
    fig = make_subplots(rows=1, cols=2, horizontal_spacing=0.05, vertical_spacing=0.05)
    fig.add_trace(go.Histogram(x=rank_df[1], histnorm='percent', name=cust_index, nbinsx=100, opacity=0.25), row=1, col=1)
    fig.add_scatter(x=[cust_prob,cust_prob], y=[0,20], mode='lines', name=customer_selected, row=1, col=1)
    fig.update_xaxes(title_text='Predicted probability (Label = 1) of Customer {}'.format(cust_index), row=1, col=1)
    fig.update_layout(barmode='overlay')
    fig.update_layout(height=400, width=2000, showlegend=False)
    fig.update_xaxes(title = "Contribution", row=1, col=2)
    fig.add_trace(go.Waterfall(name = customer_selected, orientation = "v", 
#                                   x = ['Intercept'] + feat_list[:40], y = list(intercept) + list(cust_local.iloc[0,:40])),row=1,col=2)
                                    x = feat_list[:40], y = list(cust_local.iloc[0,:40])),row=1,col=2)
    return fig

fig_up = upper_figure()
st.plotly_chart(fig_up)

st.subheader('Scores')
save = []
# global score chart and dots of local score
def lower_figure(num_rows=3, num_cols=4):
    fig = make_subplots(specs=[[{"secondary_y": True},{"secondary_y": True},{"secondary_y": True},{"secondary_y": True}],
                               [{"secondary_y": True},{"secondary_y": True},{"secondary_y": True},{"secondary_y": True}],
                               [{"secondary_y": True},{"secondary_y": True},{"secondary_y": True},{"secondary_y": True}]
                              ], 
                        rows=num_rows, cols=num_cols, horizontal_spacing=0.05, vertical_spacing=0.05)
    min_score = min(df.scores)
    max_score = max(df.scores)
    k=0
    for i in range(0,num_rows):
        for j in range(0,num_cols):
            x = df[df.col_name == feat_list[k]].names
            y = df[df.col_name == feat_list[k]].scores
            dist = raw.loc[:,feat_list[k]]      
            fig.add_trace(go.Histogram(x=dist,name=feat_list[k], histnorm='percent', nbinsx=15, opacity=0.25), 
                          secondary_y=True, row=i+1, col=j+1)
            fig.add_scatter(x=x, y=y, name=feat_list[k], mode='lines', secondary_y=False, row=i+1, col=j+1)
            fig.add_scatter(x=[dist[cust_index]], y=[cust_local.loc[cust_index,feat_list[k]]], name=customer_selected, 
                            mode='markers', marker_size=[15], marker_color=['#ff7f0e'], row=i+1, col=j+1)
            fig.update_xaxes(title_text=feat_list[k], row=i+1, col=j+1)
            fig.update_yaxes(range=[min_score, max_score], secondary_y=False, row=i+1, col=j+1)
            fig.update_yaxes(secondary_y=True,showgrid=False)
            fig.update_layout(barmode='stack',bargap=0.1)
            save.append([feat_list[k], min(x), max(x), dist[cust_index]])
            k+=1

    fig.update_layout(height=1100, width=2000, showlegend=False)
    return fig, save

fig_low, save_df = lower_figure()
st.plotly_chart(fig_low)
# if st.checkbox('Submission file'):
#     st.subheader('Final submission')
#     st.write(finaldata)

# Create row, column, and value inputs
sidebar_expander = st.sidebar.beta_expander("Simulation box")
with sidebar_expander:
   _, slider_col, _ = st.beta_columns([0.02, 30.96, 0.02])
   with slider_col:
        top1 = st.slider(save_df[0][0], int(save_df[0][1]), int(save_df[0][2]), value=int(save_df[0][3]))
        top2 = st.slider(save_df[1][0], int(save_df[1][1]), int(save_df[1][2]), value=int(save_df[1][3]))
        top3 = st.slider(save_df[2][0], int(save_df[2][1]), int(save_df[2][2]), value=int(save_df[2][3]))
        top4 = st.slider(save_df[3][0], int(save_df[3][1]), int(save_df[3][2]), value=int(save_df[3][3]))
        top5 = st.slider(save_df[4][0], int(save_df[4][1]), int(save_df[4][2]), value=int(save_df[4][3]))
        top6 = st.slider(save_df[5][0], int(save_df[5][1]), int(save_df[5][2]), value=int(save_df[5][3]))
        top7 = st.slider(save_df[6][0], int(save_df[6][1]), int(save_df[6][2]), value=int(save_df[6][3]))

model = joblib.load('../model/02_1_model_ebm_no_interaction.pickle')
simul_x = pd.DataFrame(raw.loc[cust_index,:]).transpose()
simul_x[save_df[0][0]] = top1
simul_x[save_df[1][0]] = top2
simul_x[save_df[2][0]] = top3
simul_x[save_df[3][0]] = top4
simul_x[save_df[4][0]] = top5
simul_x[save_df[5][0]] = top6
simul_x[save_df[6][0]] = top7

simul_y = pd.DataFrame(model.predict_proba(simul_x.iloc[:,2:]))
simul_y.rename({0:'Prob 0',1:'Prob 1'}, axis='columns', inplace=True)
st.sidebar.table(simul_y)
# st.sidebar.slider("Standard layout slider")


st.subheader('Solutions')
   
    



# # Change the entry at (row, col) to the given value
# df.values[row][col] = value

# # And display the result!
# st.dataframe(df)