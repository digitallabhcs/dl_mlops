import streamlit as st
import pandas as pd
import numpy as np
import plotly.graph_objects as go
import plotly.express as px
from plotly import tools
import plotly.offline as py
import plotly.figure_factory as ff
from plotly.subplots import make_subplots
import matplotlib.pyplot as plt
from streamlit_agraph import agraph, Node, Edge, Config
import joblib

st.set_page_config(
    page_title="1st App: What-if for santander transaction prediction",
    layout="wide",
    initial_sidebar_state="expanded",
)

st.title('Dashboard by ISFJ')
st.write("Let's make a minimum-viable-product")

result_data = ('../scores/01_5_scores_ebm_global.pickle')
raw_csv = '../../../../data/santander/raw/train.csv'
raw = pd.read_csv(raw_csv)

@st.cache
def load_data(DATA_URL):
    data = pd.read_pickle(DATA_URL)
    lowercase = lambda x: str(x).lower()
    data.rename(lowercase, axis='columns', inplace=True)
    return data

# df_load_state = st.text('Loading data...')
df = load_data(result_data)
# df_load_state.text("Done! Start what-if simulation!")


# final_model_path = 'xgb_total_final.pkl'
# model = joblib.load(final_model_path)
# d = pd.DataFrame(model.get_fscore().items(), columns=['feature','importance']).sort_values('importance', ascending=False)

# d = pd.read_csv(model_FI_URL)
# fig = px.bar(d.sort_values('importance',ascending=True), y='feature', x='importance', title='xgboost model', orientation='h')
# fig.update_yaxes(tickfont=dict(size=9))
# fig.update_xaxes(tickfont=dict(size=9))
# fig.update_layout(height=800, width=1200)

# finaldata = load_data(submit_file_URL, 100)
# if st.checkbox('Global scores'):
st.subheader('Understand the model')
num_rows = 3
num_cols = 4
fig = make_subplots(specs=[[{"secondary_y": True},{"secondary_y": True},{"secondary_y": True},{"secondary_y": True}],[{"secondary_y": True},{"secondary_y": True},{"secondary_y": True},{"secondary_y": True}],[{"secondary_y": True},{"secondary_y": True},{"secondary_y": True},{"secondary_y": True}]], rows=num_rows, cols=num_cols, horizontal_spacing=0.05, vertical_spacing=0.05)
# for i in range(len(feat_list)):
feat_list = df.col_name.drop_duplicates().tolist()
min_score = min(df.scores)
max_score = max(df.scores)

k=0
for i in range(0,num_rows):
    for j in range(0,num_cols):
        x = df[df.col_name == feat_list[k]].names
        y = df[df.col_name == feat_list[k]].scores
        dist = raw.loc[:,feat_list[k]]
#         counts, bins = np.histogram(dist, bins=range(int(min(dist)), int(max(dist)), 5))
#         bins = 0.5* (bins[:-1]+bins[1:])
        
        fig.add_trace(go.Histogram(x=dist,name=feat_list[k], nbinsx=15, opacity=0.25), secondary_y=True, row=i+1, col=j+1)
        fig.add_scatter(x=x, y=y, name=feat_list[k], secondary_y=False, row=i+1, col=j+1)
#         fig.add_bar(x=bins, y=counts, row=i+1, col=j+1)
        fig.update_xaxes(title_text=feat_list[k], row=i+1, col=j+1)
        fig.update_yaxes(range=[min_score, max_score], secondary_y=False, row=i+1, col=j+1)
        fig.update_yaxes(secondary_y=True,showgrid=False)
        fig.update_layout(barmode='stack',bargap=0.1)
        k+=1

fig.update_layout(height=1200, width=1800)
st.write(fig)
# if st.checkbox('Plot'):
#     st.subheader('Check the inference')
#     fig = px.scatter(df, 
#         x="cf",
#         y="pred_prob",
#         color="target",
#     )
#     fig.update_layout(
#         xaxis_title="cf",
#         yaxis_title="inference",
#     )
#     st.write(fig)
# if st.checkbox('Submission file'):
#     st.subheader('Final submission')
#     st.write(finaldata)

    

# add_selectbox = st.sidebar.selectbox("Select Box", ("EDA", "Modeling result", "Hyperparemter tuning"))
    
# st.subheader('Number of pickups by hour')
# hist_values = np.histogram(data[DATE_COLUMN].dt.hour, bins=24, range=(0,24))[0]
# st.bar_chart(hist_values)

# hour_to_filter = st.slider('hour', 0, 23, 17)
# filtered_data = data[data[DATE_COLUMN].dt.hour == hour_to_filter]

# st.subheader('Map of all pickups at %s:00' % hour_to_filter)
# st.map(filtered_data)




# import numpy as np
# import pandas as pd

# # Randomly fill a dataframe and cache it
# @st.cache(allow_output_mutation=True)
# def get_dataframe():
#     return pd.DataFrame(
#         np.random.randn(50, 20),
#         columns=('col %d' % i for i in range(20)))


# df = get_dataframe()

# # Create row, column, and value inputs
# row = st.number_input('feature', max_value=df.shape[0])
# col = st.number_input('instance', max_value=df.shape[1])
# value = st.number_input('value')

# # Change the entry at (row, col) to the given value
# df.values[row][col] = value

# # And display the result!
# st.dataframe(df)