import pandas as pd
from interpret.glassbox import ExplainableBoostingRegressor



def append_prediction(X: pd.DataFrame, 
                      model) -> pd.DataFrame:
    '''
    This function appends predictions using specified trained model to input train dataset.
    Put in train dataset and trained model which has "predict" method.
    
    [Usage]
    > appended_X=append_prediction(X, model)
    > print(appended_X)
    
    >>  area	sale_condition	total_s_f	prediction
        0	896		0	1778.0	122522.794802
        1	1329	0	2658.0	172328.394113
    
    '''
    X["prediction"]=model.predict(X)
    return X




def get_feature_importance_df(X: pd.DataFrame,
                  ebm_model: ExplainableBoostingRegressor) -> pd.DataFrame:
    '''
    This function returns a dataframe with feature importance information.
    Use train dataset and trained EBM model as inputs.
    
    [Usage]
    > fi=get_feature_importance_df(X, ebm)
    > print(fi)
    
    >>    feature	importance
    0	area	4334.164235
    1	m_s_sub_class	2277.138013
    2	m_s_zoning	1782.596624
    3	lot_frontage	1175.695527
        ...
    
    '''
    
    # define ebm_global 
    ebm_global = ebm_model.explain_global(name = 'EBM')
    
    num_feats = X.shape[1]
    glob_internal = ebm_global._internal_obj
    # glob_internal_overall==ebm_global.data() --> True
    glob_internal_overall=glob_internal['overall']
    
    # append feature importance information feature by feature
    feature_importance=[]
    for i in range(0,num_feats):
        feat=glob_internal_overall['names'][i]
        score=glob_internal_overall['scores'][i]
        row=[feat, score]
        feature_importance.append(row)
    
    # aggregate all and return
    df_feature_importance = pd.DataFrame(feature_importance, columns=['feature','importance'])    

    return df_feature_importance




def get_global_df(X: pd.DataFrame,
                  ebm_model: ExplainableBoostingRegressor) -> pd.DataFrame:
    
    '''
    This function makes a dataframe which contains global feature information.
    Use train dataset and trained EBM model as inputs.
    This returns a concatenated dataframe that consists of scores/upper bounds/lower bounds of each feature.
    
    [Usage]
    
    > global_df=get_global_df(X, ebm)
    > print(global_df)
    
    >>    names        scores  upper_bounds  lower_bounds   col_name
    0     334.0  -7297.398076  -5662.137572   -8932.65858       area
    1     608.5  -6017.646703  -5273.323326  -6761.970079       area
    ..      ...           ...           ...           ...        ...                              
    254  5085.0  56985.735655  62221.625117  51749.846193  total_s_f
    255  6872.0  56985.735655  62221.625117  51749.846193  total_s_f
    
    '''
    
    # define ebm_global 
    ebm_global = ebm_model.explain_global(name = 'EBM')
    
    # to exclude interaction features, use columns and column size from train dataset
    num_feats = X.shape[1]

    df_feature_contributions = []
    
    # to exclude features with object data type and with binary values, check every features 
    # the reason to exclude the features is these may create NAs with treatement which appends an extra value to pandas series
    object_cols= X.select_dtypes("object").columns.tolist()

    binary_cols=[]
    for col in X.columns:
        val_cnt=X[col].value_counts()
        # check whether it contains only binary values or not
        if val_cnt.index.shape[0]==2:
            binary_cols.append(col)

    excld_cols=object_cols+binary_cols
    
    # extract internal object from ebm_global instance
    glob_internal = ebm_global._internal_obj
    # ebm_global._internal_obj['overall'] object is equal to the result of ebm_global.data() method
    glob_internal_overall=glob_internal['overall']

    for i in range(0,num_feats):
        # get explanation values of each feature
        glob_internal_values=glob_internal['specific'][i]
        # get feature name to specify it in the final dataframe
        feature_name = glob_internal_overall['names'][i]

        # get unique values from a feature
        names = glob_internal_values['names']
        
        
        # if the feature is found in column list to exclude, skip appending process. 
        if feature_name in excld_cols:
            scores = pd.Series(glob_internal_values['scores'])
            ub = pd.Series(glob_internal_values['upper_bounds'])
            lb = pd.Series(glob_internal_values['lower_bounds'])        

        else:

            scores = pd.Series(glob_internal_values['scores']).append(pd.Series([glob_internal_values['scores'][-1]]), ignore_index=True)
            ub = pd.Series(glob_internal_values['upper_bounds']).append(pd.Series([glob_internal_values['upper_bounds'][-1]]), ignore_index=True)
            lb = pd.Series(glob_internal_values['lower_bounds']).append(pd.Series([glob_internal_values['lower_bounds'][-1]]), ignore_index=True)

        # make the result in the form of a DataFrame object
        df_feature_contribution = pd.DataFrame([names,scores,ub,lb]).T
        df_feature_contribution.columns = ['names','scores','upper_bounds','lower_bounds']
        
        # specify the feature name, to handle by its name in the future work
        df_feature_contribution["col_name"]=feature_name

        # append it to the result list
        df_feature_contributions.append(df_feature_contribution)
        
        # concatenate and return
    return pd.concat(df_feature_contributions, axis=0)





def get_score_variance_table(global_df: pd.DataFrame,
                             ebm_model: ExplainableBoostingRegressor,
                            exclude_extreme_ratio: float=0.05) -> pd.DataFrame:
    
    '''
    By using global_df(created by get_global_df function), create score variance table which describe score variance of each feature
    
    [Usage]
    
    > score_variance_table=get_score_variance_table(global_df, ebm)
    > print(score_variance_table)
        
    >> col_name min	max	max_min_gap	max_min_gap_excld_extrm	feature_importance	std
    area	-7297.398076	44731.874084	52029.272160	20697.855751	4334.164235	7376.704911
    bedroom_abv_gr	-5688.261279	4741.692629	10429.953908	8226.317146	721.284098	3803.859622
    bldg_type	-6637.518177	741.456291	7378.974467	2658.049948	1226.936684	2549.203883 
    
    
    '''
    
    
    # get min/max score values from each values and save it as pandas series
    min_series=global_df.groupby("col_name").apply(lambda x: x.scores.min())
    max_series=global_df.groupby("col_name").apply(lambda x: x.scores.max())
    
    # get max-min gap series
    max_min_series=max_series-min_series
    
    # lambda function for max-min excluding extreme values
    def max_min_excld_extrm_vals(x, col="", percent=exclude_extreme_ratio):
        top_05=x[col].quantile(1-percent)
        low_05=x[col].quantile(percent)
        x[col]=x[col][x[col]<=top_05]
        x[col]=x[col][x[col]>=low_05]
        return x[col].max()-x[col].min()

    max_min_excld_extrm_vals_series=global_df.groupby("col_name").apply(max_min_excld_extrm_vals,col="scores", percent=exclude_extreme_ratio)
    
    # concat all pandas series
    score_variance_table=pd.concat([min_series,max_series,max_min_series, max_min_excld_extrm_vals_series],axis=1)
        
    # get feature importance data from a trained EBM model instance
    FI_df=pd.DataFrame(list(zip(ebm_model.feature_names, ebm_model.feature_importances_)))
    # set first(0) column as an index
    FI_df=FI_df.set_index(0)
    
    # merge tables
    score_variance_table=score_variance_table.merge(FI_df, right_index=True, left_index=True, how="left")
    # name columns
    score_variance_table.columns=["min","max","max_min_gap","max_min_gap_excld_extrm","feature_importance"]
    
    # standard deviation
    score_variance_table["std"]=global_df.groupby("col_name").apply(lambda x: x.scores.std())
    
    # fill NA which is made while processing max_min_excld_extrm_vals method
    score_variance_table.max_min_gap_excld_extrm=score_variance_table.max_min_gap_excld_extrm.fillna(0)
    
    return score_variance_table







def pygam_statement(num_featuresL: int,
                    num_cate_features: int,
                    cate_includeL:bool = False):
    
    '''
    Return pygam statement which is used as an input for pyGAM instance.
    Put in number of features(num_features) and number of categorical features(num_cate_features), and specify whether you would include categorical features or not.
    Please note that num_features argument should be larger than num_cate_features argument.
    
    
    [Usage]
    
    > pygam_statement(6, 3, True)
    >> s(0) + s(1) + s(2) + f(3) + f(4) + f(5)
    
    > gam=LinearGAM(statement)
    > gam.fit(X, y)
    >> LinearGAM(callbacks=[Deviance(), Diffs()], fit_intercept=True, 
   max_iter=100, scale=None, 
   terms=s(0) + s(1) + s(2) + f(3) + f(4) + f(5) + intercept,
   tol=0.0001, verbose=False)
    
    '''
    
    
    
    statement_list=[f"s({i})+" if i!=num_features-num_cate_features-1 else f"s({i})" for i in range(num_features-num_cate_features)]

    # if argument for categorical features is True, create statement list for categorical features and append it to the end of original list
    if cate_include:
        statement_list_cate=["+"]+[f"f({i})+" if i!=num_features-1 else f"f({i})" for i in range(num_features-num_cate_features,num_features)]
        statement_list=statement_list+statement_list_cate
        statement=eval("".join(statement_list))
    
    else:   
        statement=eval("".join(statement_list))
        
    return statement



def pygam_feature_plot(plot_shape: tuple,
                       pygam_model,
                       X_colnames: list or pd.Series) -> None:
    
    '''
    Plot partial dependence plot for each feature. 
    The number of plots will be (plot_shape[0]*plot_shape[1]), from column number 0 to column number (plot_shape[0]*plot_shape[1]).
    
    As inputs, specify shape of plots you want to display and fitted pygam model.
    To provide titles to the plots, put in feature name list from the train dataset.
    
    
    [Usage]
    > pygam_feature_plot((5,5), gam, X.columns)
    
    
    '''
    
    plot_shape_y=plot_shape[0]
    plot_shape_x=plot_shape[1]
    
    ## plotting
    plt.figure(figsize=(50,20));

    fig, axs = plt.subplots(plot_shape_y,plot_shape_x, constrained_layout=True, figsize=(24,13.5));
    plt.subplots_adjust(left=0.125, bottom=0.1, right=0.9, top=0.9, wspace=0.5, hspace=0.5)

    # X_colnames = X.columns.tolist()

    for num, ax in enumerate(axs):
        for j in range(plot_shape_x):
            
            chart_num=num*plot_shape_x+j

            XX = pygam_model.generate_X_grid(term=chart_num)
            ax[j].plot(XX[:, chart_num], pygam_model.partial_dependence(term=chart_num, X=XX))
            ax[j].plot(XX[:, chart_num], pygam_model.partial_dependence(term=chart_num, X=XX, width=.95)[1], c='r', ls='--')
            if chart_num == 0:
                ax[j].set_ylim(-1000,1000)
            ax[j].set_title(X_colnames[chart_num])
            
    return None


def get_local_df(sample_X: pd.DataFrame,
                 sample_y: pd.Series,
                ebm_model: ExplainableBoostingRegressor):
    
    '''
    This function returns a dataframe containing local scores which can be used for futher application such as visualization.
    It accepts ebm model and sample X & y as inputs.
    Cut a part out of train dataset and use it as sample X & y.
    
    [Usage]
    > local_df=get_local_df(ebm, X.iloc[:10], y.iloc[:10])
    > print(local_df)
    
        area	m_s_sub_class	val_name	predicted_score		actual_score	intercept_score
    0	-5048.064963	-1427.342455	scores	122522.794802	105000	179830.827042
    1	896.000000	20	values	122522.794802	105000	179830.827042

    '''
    
    
    ebm_local = ebm_model.explain_local(sample_X, sample_y, name='EBM')

    local_internal_obj=ebm_local._internal_obj
    local_info_list=[]
    local_data=local_internal_obj["specific"]
    

    for i in range(len(local_data)):
        
        col_names=sample_X.columns.tolist()
        
        tar=local_data[i]

        scores=tar['scores'][:len(col_names)]
        values=tar['values'][:len(col_names)]

        intercept_score=tar['extra']["scores"][0]
        actual_score=tar['perf']['actual_score']
        predicted_score=tar['perf']['predicted_score']

        col_names.append("val_name")

        empty=pd.DataFrame(columns=col_names)

        scores.append("scores")

        empty.loc[0]=scores

        values.append("values")

        empty.loc[1]=values


        empty["predicted_score"]=predicted_score
        empty["actual_score"]=actual_score
        empty["intercept_score"]=intercept_score

        local_info_list.append(empty)
        
    local_df=pd.concat(local_info_list,axis=0)
    local_df=local_df.reset_index(drop=True)
    return local_df