#!/usr/bin/env python
# coding: utf-8

# In[ ]:

import pandas as pd 
import numpy as np
    
def ebm_pred_result(model, X, y):
    '''
    Fuction to create dataframe of EBM prediction result.
    
    * params :
    ---------------------------
      model: fitted EBM object 
      X: dataframe of features
      y: series of target value
    
    
    * returns: 
    ---------------------------
      For regression: 
          df_ebm_pred (y_true, y_pred, features)
          
      For classification:        
          df_ebm_pred (y_score, y_true, y_pred, class(TP/TN/FP/FN), features)
    '''
    
    # For regression
    if (y.dtype == int) or (y.dtype == float):
        
        y_true = pd.DataFrame(y)
        y_true = y_true.rename({y_true.columns[0]:'y_true'},axis=1)
        y_pred = np.round(ebm.predict(X), 1)
        y_pred = pd.DataFrame(y_pred).rename({0:'y_pred'},axis=1)
        
        df_tmp = pd.concat([y_true, y_pred], axis=1)
        
        df_ebm_pred = pd.concat([df_tmp, X.reset_index(drop=True)], axis=1)
        
        return df_ebm_pred
    
    # For classification
    else: 
        target = pd.DataFrame(data=y).columns.tolist()

        y_score = pd.DataFrame(data=model.predict_proba(X)).\
                            rename({0:'y_proba_0', 1:'y_proba_1'}, axis=1)
        y_true = pd.DataFrame(data=y).reset_index(drop=True)
        y_true = y_true.rename({y_true.columns[0]:'y_true'}, axis=1)
        y_pred = pd.DataFrame(data=model.predict(X)).rename({0:'y_pred'}, axis=1)

        df = pd.concat([y_score, y_true, y_pred], axis=1)

        # Classification Result
        conditionlist = [
            (df['y_true']==1) & (df['y_pred']==1),
            (df['y_true']==0) & (df['y_pred']==0),
            (df['y_true']==0) & (df['y_pred']==1),
            (df['y_true']==1) & (df['y_pred']==0)]

        choicelist = ['TP', 'TN', 'FP', 'FN']

        df['class'] = np.select(conditionlist, choicelist, default='Not Specified')
        df_ebm_pred = pd.concat([df, X.reset_index(drop=True)], axis=1)

        return df_ebm_pred