import altair as alt
import pandas as pd
import numpy as np

def y_dist_figure(df, target, barsize, x_title):
    x_col = target + ":O"
    fig = alt.Chart(df).mark_bar(
        size=barsize,
        cornerRadiusTopLeft=3,
        cornerRadiusTopRight=3
        ).encode(
            x=alt.X(x_col, title=""),
            y=alt.Y('count():Q', title=""),
            # y=alt.Y('count():Q', axis=alt.Axis(title='#.instances')),
            color=alt.Color(target, legend=None)
        ).properties(
            width=400,
            height=400
        ).configure_axisX(
            labelAngle=-0,
        ).configure_axis(
            grid=False,
            labelPadding=15,
            labelFontSize=14
        )

    return fig

def feature_importance_figure(df, barsize):
    fig = alt.Chart(df).mark_bar(
        size=barsize,
        cornerRadiusTopRight=3,
        cornerRadiusBottomRight=3
        ).encode(
            x=alt.X('importance:Q', title=""),
            y=alt.Y("feature:O", title="", sort='-x'),
            color=alt.Color('importance', legend=None),
        ).properties(
            height=510, 
            width=480
        ).configure_axis(
            grid=False,
            labelPadding=15,
            labelFontSize=14
        )
        
    return fig

def ebm_score_figure(df, feature, global_df, local_df, instance, height, width):
    
    score = pd.DataFrame({
    'feature': global_df[global_df.col_name==feature].loc[:,'pdp_x_value'],
    'score': global_df[global_df.col_name==feature].loc[:,'score'],
    })
    score_ub = pd.DataFrame({
    'feature': global_df[global_df.col_name==feature].loc[:,'pdp_x_value'],
    'score': global_df[global_df.col_name==feature].loc[:,'upper_bound'],
    })
    score_lb = pd.DataFrame({
    'feature': global_df[global_df.col_name==feature].loc[:,'pdp_x_value'],
    'score': global_df[global_df.col_name==feature].loc[:,'lower_bound'],
    })
    local_score = pd.DataFrame({
    'value': local_df.loc[:,feature],
    'score': local_df.loc[:,feature+'_scores'],
    })
    zero_line = pd.DataFrame({
    'value': [min(global_df[global_df.col_name==feature].loc[:,'pdp_x_value']), max(global_df[global_df.col_name==feature].loc[:,'pdp_x_value'])],
    'score': [0,0],
    })

        
    chart_mean = alt.Chart(score).mark_line(interpolate='step-after').encode(
        x=alt.X('feature', title=""),
        y=alt.Y('score', title=""),
        color=alt.value('#358CE1')
    ).properties(
        width=width,
        height=height
    )

    chart_ub = alt.Chart(score_ub).mark_line(interpolate='step-after').encode(
        x='feature',
        y='score',
        color=alt.value("#7BCDCD")
    ).properties(
        width=width,
        height=height
    )

    chart_lb = alt.Chart(score_lb).mark_line(interpolate='step-after').encode(
        x='feature',
        y='score',
        color=alt.value("#7BCDCD")
    ).properties(
        width=width,
        height=height
    )

    dashed_line = alt.Chart(zero_line).mark_line(strokeDash=[8,8]).encode(
        x='value',
        y='score',
        color=alt.value('#CFD8DC'),
    ).properties(
        width=width,
        height=height
    )

    hist = alt.Chart(df.sample(frac=0.5)).mark_bar(size=7,
            fillOpacity=0.2,
    ).encode(
        x=alt.X(feature, bin=alt.Bin(maxbins=50)),
        y=alt.Y('count()', title=""),
        color=alt.value("#708090")
    ).properties(
        width=width,
        height=height
    )
    

    
    local_point = alt.Chart(local_score.iloc[instance:instance+1,:]).mark_circle().encode(
        x='value',
        y='score',
        color=alt.value("#FFA726"),
        size=alt.value(100)
    )

    fig = alt.layer(dashed_line+chart_ub+chart_lb+chart_mean+local_point, hist).resolve_scale(
        y = 'independent'
    ).configure_axis(
        grid=False,
        labelPadding=20
    ).properties(
        title = feature,
        # padding=10,
        autosize=alt.AutoSizeParams(
            type='pad',
            contains='padding'
        )
    ).configure_title(
        fontSize=20,
        font='avenir',
        anchor='start',
        color='black',
        offset=20,
        dx=-10,

    )
    return fig


def contribution_figure(df, local_df, instance, height, width):
    
    feature_list = df.iloc[:,:-1].columns.tolist()
    
    score_list = [f + '_scores' for f in feature_list]
    
    score_df = pd.DataFrame({
    'feature': feature_list,
    'score': local_df.loc[instance, score_list]
    }).sort_values(by='score', key=abs, ascending=False).reset_index()

    intercept_df = pd.DataFrame({
    'feature': score_df['feature'],
    'score': local_df.intercept_score[:len(feature_list)],
    'abs_score': pd.Series(np.abs(score_df['score'])),
    })

    df_plot = pd.DataFrame({
    'start': pd.Series(np.cumsum(score_df['score']))+intercept_df['score'].values-score_df['score'],
    'end': pd.Series(np.cumsum(score_df['score']))+intercept_df['score'].values,
    'cat': ['decrease' if s else 'increase' for s in list(score_df['score'] < 0)],
    'feature': score_df['feature'],
    'abs_score': pd.Series(np.abs(score_df['score'])),
    })

    color_ = ['#CFD8DC', '#78909C']
    domain_ = ['decrease', 'increase']

    chart = alt.Chart(df_plot).mark_bar(size=25).encode(
        x=alt.X('feature:N', title="", 
                sort=alt.EncodingSortField(field='abs_score', order='descending')),
        y=alt.Y('start', title="",
                scale=alt.Scale(domain=(min(df_plot.start),max(df_plot.start)+0.05))),
        y2='end',
        color=alt.Color('cat', scale=alt.Scale(domain=domain_, range=color_), legend=None),
    ).transform_window(
        rank='rank(abs_score)',
        sort=[alt.SortField('abs_score', order='descending')]
    ).properties(
        width=width,
        height=height
    )

    dashed_line = alt.Chart(intercept_df).mark_line(strokeDash=[8,8]).encode(
        x=alt.X('feature', 
                sort=alt.EncodingSortField(field='abs_score', order='descending')),
        y='score',
        color=alt.value('#CFD8DC'),
    ).properties(
        width=width,
        height=height
    )

    fig = alt.layer(chart+dashed_line).resolve_scale().configure_axis(
        grid=False,
        labelPadding=20
    ).properties(
        # title='Instance {}'.format(instance),
        # padding=10,
        autosize=alt.AutoSizeParams(
            type='pad',
            contains='padding'
        )
    ).configure_title(
        fontSize=20,
        font='avenir',
        anchor='start',
        color='black',
        offset=20,
        dx=-10,
    )

    return fig

def ebm_pdp_figure(df, feature, global_df, height, width):
    
    score = pd.DataFrame({
    'feature': global_df[global_df.col_name==feature].loc[:,'pdp_x_value'],
    'score': global_df[global_df.col_name==feature].loc[:,'score'],
    })
    score_ub = pd.DataFrame({
    'feature': global_df[global_df.col_name==feature].loc[:,'pdp_x_value'],
    'score': global_df[global_df.col_name==feature].loc[:,'upper_bound'],
    })
    score_lb = pd.DataFrame({
    'feature': global_df[global_df.col_name==feature].loc[:,'pdp_x_value'],
    'score': global_df[global_df.col_name==feature].loc[:,'lower_bound'],
    })
    zero_line = pd.DataFrame({
    'value': [min(global_df[global_df.col_name==feature].loc[:,'pdp_x_value']), max(global_df[global_df.col_name==feature].loc[:,'pdp_x_value'])],
    'score': [0,0],
    })

        
    chart_mean = alt.Chart(score).mark_line(interpolate='step-after').encode(
        x=alt.X('feature', title=""),
        y=alt.Y('score', title=""),
        color=alt.value('#358CE1')
    ).properties(
        width=width,
        height=height
    )

    chart_ub = alt.Chart(score_ub).mark_line(interpolate='step-after').encode(
        x='feature',
        y='score',
        color=alt.value("#7BCDCD")
    ).properties(
        width=width,
        height=height
    )

    chart_lb = alt.Chart(score_lb).mark_line(interpolate='step-after').encode(
        x='feature',
        y='score',
        color=alt.value("#7BCDCD")
    ).properties(
        width=width,
        height=height
    )

    dashed_line = alt.Chart(zero_line).mark_line(strokeDash=[8,8]).encode(
        x='value',
        y='score',
        color=alt.value('#CFD8DC'),
    ).properties(
        width=width,
        height=height
    )

    hist = alt.Chart(df.sample(frac=0.5)).mark_bar(size=7,
            fillOpacity=0.2,
    ).encode(
        x=alt.X(feature, bin=alt.Bin(maxbins=50)),
        y=alt.Y('count()', title=""),
        color=alt.value("#708090")
    ).properties(
        width=width,
        height=height
    )
    

    fig = alt.layer(dashed_line+chart_ub+chart_lb+chart_mean, hist).resolve_scale(
        y = 'independent'
    ).configure_axis(
        grid=False,
        labelPadding=20
    ).properties(
        title = feature,
        # padding=10,
        autosize=alt.AutoSizeParams(
            type='pad',
            contains='padding'
        )
    ).configure_title(
        fontSize=20,
        font='avenir',
        anchor='start',
        color='black',
        offset=20,
        dx=-10,

    )
    return fig