import psycopg2.extras as extras
import snowflake.connector
from io import StringIO
import numpy as np
import psycopg2
import os
import sys


'''
param_dic = {
    "host"      : "3.137.19.253",
    "database"  : "mlops",
    "user"      : "dlab",
    "password"  : "dlab",
    "port"      : "5432"
}
'''


def connect(param_dic):
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**param_dic)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        sys.exit(1) 
    print("connect successful")
    return conn


def snowflake_connect(param_dic):
    """ Connect to the snowflake database server """
    snowflake.connector.paramstyle = 'qmark'
    conn = None
    try:
        # connect to the PostgreSQL server
        print('Connecting to the snowflake database...')
        conn = snowflake.connector.connect(**param_dic)
    except (Exception, snowflake.connector.errors.ProgrammingError) as error:
        print(error)
        sys.exit(1)
    print("snowflake_connect successful")
    return conn


def execute_values(conn, df, table):
    """
    Using psycopg2.extras.execute_values() to insert the dataframe
    """
    # Create a list of tupples from the dataframe values
    tuples = [tuple(x) for x in df.to_numpy()]
    # Comma-separated dataframe columns
    cols = ','.join(list(df.columns))
    # SQL quert to execute
    query  = "INSERT INTO %s(%s) VALUES %%s" % (table, cols)
    cursor = conn.cursor()
    try:
        extras.execute_values(cursor, query, tuples)
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        conn.rollback()
        cursor.close()
        return 1
    print("execute_values() done")
    cursor.close()


def snowflake_execute_values(conn, df, table):
    """
    Using psycopg2.extras.execute_values() to insert the dataframe
    """
    snowflake.connector.paramstyle = 'qmark'
    colnum = df.shape[1]
    # Create a list of tupples from the dataframe values
    tuples = [tuple(x) for x in df.to_numpy()]
    # Comma-separated dataframe columns
    cols = ','.join(list(df.columns))
    # values
    values = ''
    for i in range(0, colnum - 1):
        values += '?,'  # append 과 같은 속도 자랑
    values += '?'
    # SQL quert to execute
    query  = "INSERT INTO %s(%s) VALUES (%s)" % (table, cols, values)
    cursor = conn.cursor()
    try:
        cursor.executemany(query, tuples)
        conn.commit()
    except (Exception, snowflake.connector.errors.ProgrammingError) as error:
        print("Error: %s" % error)
        conn.rollback()
        cursor.close()
        return 1
    print("snowflake_execute_values() done")
    cursor.close()