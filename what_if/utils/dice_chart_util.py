import numpy as np
import pandas as pd
import altair as alt

def expected_score_figure(df_pred, idx_cf=0):
    source = round(df_pred,1)
    source['sort'].iloc[idx_cf+1] = 'expected'
    source

    line_pred =  alt.Chart(source).mark_line(color='#A5CCDD'
                   ).encode(
                       x = alt.X('pred_score:Q', 
                                 scale=alt.Scale(domain=[3, 9]),
                                 axis=alt.Axis(grid=False), title=None),
                )

    # Add points for life expectancy in 1955 & 2000
    point = alt.Chart(source).mark_point(
        size=100,
        opacity=1,
        filled=True
    ).encode(
        x=alt.X('pred_score:Q',scale=alt.Scale(domain=[3, 9]), title=None),
        color=alt.Color('pred_score:Q',
            scale=alt.Scale(
                range=["#CFD8DC", "#36B9DD"]
            )
        )
    )

    points = alt.Chart(source).mark_point().encode(
        x='x:Q',
        y='y:Q'
    )

    text = point.mark_text(
        align='center',
        baseline='top',
        dx=0,
        dy=-20
    ).encode(
        text='sort'
    )

    fig = (line_pred+point+text).properties(
                       height=100, 
                       width=500,
#                        padding=50,
    ).encode(
                      alt.Tooltip(['sort','pred_score']))
    
    return fig


def counterfactual_figure(df_sparse_cfs):
    source = round(df_sparse_cfs,1)
    max_value = max(df_sparse_cfs['value_org'])*1.1
    scale = alt.Scale(domain=[0, max_value])

    bar_org =  alt.Chart(source).mark_bar(
                       size=22,
                       cornerRadiusTopRight=3, cornerRadiusBottomRight=3, 
                       fontSize=10,
                       color = "#E9EAEC"
                   ).encode(
                       # x=alt.X('feature:N', sort='-y', title=None),
                       y = alt.Y('feature:N', title=None, sort='-x'),
                       x = alt.X('value_org:Q', axis=alt.Axis(grid=False), scale=scale, title=None),
                    )

    bar_org_2 =  alt.Chart(source).mark_bar(
                       size=22,
    #                    cornerRadiusTopRight=3, cornerRadiusBottomRight=3, 
                       fontSize=10,
                       color = "#E9EAEC"
                   ).encode(
                       # x=alt.X('feature:N', sort='-y', title=None),
                       y = alt.Y('feature:N', title=None, sort='-x'),
                       x = alt.X('end_for_org:Q', axis=alt.Axis(grid=False), scale=scale, title=None),
                    )


    bar_cf_plus = alt.Chart(source).mark_bar(
                       size=21,
                       cornerRadiusTopRight=300, cornerRadiusBottomRight=300, 
                       fontSize=10,
                       color = "#77DDF9"
                   ).encode(
                       # x=alt.X('feature:N', sort='-y', title=None),
                       y = alt.Y('feature:N', title=None, sort='-x'),
                       x = alt.X('value_cf_plus:Q', axis=alt.Axis(grid=False), scale=scale, title=None),
                    )


    bar_cf_minus = alt.Chart(source).mark_bar(
                       size=22,
                       cornerRadiusTopLeft=300, cornerRadiusBottomLeft=300, 
                       fontSize=10,
                       color = "#FFA0A3"
                   ).encode(
                       # x=alt.X('feature:N', sort='-y', title=None),
                       y = alt.Y('feature:N', title=None, sort='-x'),
                       x = alt.X('endpoint_for_minus:Q', axis=alt.Axis(grid=False), title=None),
                       x2 = alt.X2('value_cf_minus:Q')
                    )

    tick_plus = alt.Chart(source).mark_tick(
        color='#36B9DD',
        thickness=2,
        size=30 * 0.9,  # controls width of tick.
    ).encode(
        x='value_cf_plus',
        y='feature'
    )


    tick_minus = alt.Chart(source).mark_tick(
        color = "#F67077",
        thickness=2,
        size=30 * 0.9,  # controls width of tick.
    ).encode(
        x='value_cf_minus',
        y='feature'
    )

    text_org = bar_org_2.mark_text(
        align='left',
        baseline='middle',
        color='#CFD8DC',
        fontSize=15, 
        dx=3  # Nudges text to right so it doesn't appear on top of the bar
    ).encode(
        text='end_for_org:Q'
    )

    text_plus = bar_cf_plus.mark_text(
        align='left',
        baseline='middle',
        color='#36B9DD',
        fontSize=15, 
        dx=3  # Nudges text to right so it doesn't appear on top of the bar
    ).encode(
        text='value_cf_plus:Q'
    )

    text_minus = tick_minus.mark_text(
        align='center',
        baseline='middle',
        color = "#E43D40",
        fontSize=15, 
        dx=-3  # Nudges text to right so it doesn't appear on top of the bar
    ).encode(
        text='value_cf_minus:Q'
    )

    fig = (bar_cf_plus+bar_org+bar_cf_minus+tick_minus+tick_plus+text_org+text_plus+text_minus).properties(
                       height=500, 
                       width=800,
#                        padding=50
    ).configure_axis(labelFontSize=15, labelPadding=15)
    
    return fig