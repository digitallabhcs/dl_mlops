                                                               import pandas as pd
import numpy as np                             ㅍ 
import dice_ml
from dice_ml import Dice
seed=777 


def tabularize_org_cfs(df_X, idx_X, model, dice_explainer,
                       desired_score=None, total_cfs=3, 
                       random_seed=seed):

    df_org = df_X.iloc[[idx_X]]
    e = dice_explainer.generate_counterfactuals(
        query_instances=df_org,
        total_CFs=total_cfs,
        desired_range=[desired_score, desired_score + 1],
        random_seed=seed)

    df_cfs = e.cf_examples_list[0].final_cfs_df

    return df_org, df_cfs


def tabularize_value_gap(df_org, df_cfs):

    df_cfs = df_cfs.iloc[:,:-1]

    # Original feature values
    org_idx = df_org.index[0]
    df_org = df_org.rename({org_idx:'value_org'}, axis=0).T

    # CF's feature values
    df_cfs_tmp = df_cfs.copy().reset_index().rename({'index':'cf_no'},axis=1)
    df_cfs_melt = pd.melt(df_cfs_tmp, id_vars=['cf_no'])

    #Concat two dataframes
    df_gap = pd.merge(left=df_org, right=df_cfs_melt,
                  left_on=df_org.index, right_on=['variable'],
                  how='outer')
    df_gap.rename({'value':'value_cf'}, axis=1, inplace=True)
    df_gap['gap'] = np.round(df_gap['value_cf']-df_gap['value_org'], 1)
    df_gap = df_gap.sort_values(by=['cf_no','gap'], 
                            ascending=[True,False])
    df_gap = df_gap.loc[df_gap['gap']!=0]
    df_gap = df_gap.rename({'variable':'feature'},axis=1)
    df_gap = df_gap[['cf_no','feature','value_org','value_cf','gap']]
    df_gap.reset_index(drop=True, inplace=True)

    return df_gap


def tabularize_dice_pred_result(df_org, df_cfs, model):
    pred_org = np.round(model.predict(df_org),1)
    pred_cfs = np.round(model.predict(df_cfs.iloc[:,:-1]),1)

    df_pred_org = pd.DataFrame(data=pred_org, index=['current'], columns=['pred_score'])
    df_pred_cfs = pd.DataFrame(data=pred_cfs, columns=['pred_score'])
    df_pred = pd.concat([df_pred_org, df_pred_cfs], axis=0)
    df_pred = df_pred.reset_index().rename({'index':'sort'}, axis=1)
    return df_pred


def tabularize_sparse_cfs(df_org, df_cfs):
    cf_no = []
    feature = []
    value_org = []
    value_cf = []

    for i in range(len(df_cfs)):
        for c in df_org.columns:
            if all(df_org[c] == df_cfs[c].iloc[i]):
                cf_no.append(i)
                feature.append(c)
                value_org.append(df_org[c].iloc[0])
                value_cf.append(0)
            else: 
                cf_no.append(i)
                feature.append(c)
                value_org.append(df_org[c].iloc[0])
                value_cf.append(df_cfs[c].iloc[i])

    cf_no = pd.DataFrame(cf_no, columns=['cf_no'])
    feature = pd.DataFrame(feature, columns=['feature'])
    value_org = pd.DataFrame(value_org, columns=['value_org'])
    value_cf = pd.DataFrame(value_cf, columns=['value_cf'])

    df_cfs_sparse = pd.concat([cf_no, feature, value_org, value_cf], axis=1)
    df_cfs_sparse.loc[df_cfs_sparse['value_org'] < df_cfs_sparse['value_cf'], 'value_cf_plus'] = df_cfs_sparse['value_cf']
    df_cfs_sparse.loc[((df_cfs_sparse['value_org'] > df_cfs_sparse['value_cf']) & df_cfs_sparse['value_cf'] != 0), 'value_cf_minus'] = df_cfs_sparse['value_cf']
    df_cfs_sparse.loc[~df_cfs_sparse['value_cf_minus'].isna(), 'endpoint_for_minus'] = df_cfs_sparse['value_org']

    # df_cfs_sparse = df_cfs_sparse.fillna(value=0)

    return df_cfs_sparse


def calculate_max_score(df_X, idx_X, model, dice_explainer,
                       total_cfs=1, 
                       step=0.1,
                       seed=777):
    
    X = df_X.iloc[[idx_X]]
    y_pred = model.predict(X)
    min_range = np.round(y_pred, 1) + 0.1
    max_range = np.max(model.predict(df_X))
    iteration = np.round(np.arange(min_range, max_range, step),1)

    for i in iteration:        
        e = dice_explainer.generate_counterfactuals(query_instances=X, 
                                         total_CFs=total_cfs,
                                         desired_range=[i,i+0.1],
                                         random_seed=seed) 

        try:
            if len(e.cf_examples_list[0].final_cfs_df) !=0:
                pass
                
        except TypeError:
            print('{} is max score which is changable.'.format(i))
            max_score = i
            return max_score
            break