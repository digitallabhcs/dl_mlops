**readme**


1. Directory Structure

```

what_if
│
├── data
│	├── raw
│	└── processed
│	
├── docs
│	
│
├── notebooks
│	└── old_versions
│
├── utils
│
└── readme.md (this file)

```  


2. 폴더 관리 규칙  

```
	1) data  
        - 노트북/데이터 분리를 위해 별도의 폴더 사용  
        - raw: 전처리 전 상태의 데이터 파일 저장  
        - processed: 노트북 결과물로 산출된 csv, pickle 등의 데이터 저장  

	2) docs  
		- 프로젝트 관련 문서 정리  
        
	3) notebooks  
		- 주피터 노트북을 정리하는 공간  
		- old_versions: 사용하지 않는 버전, 공유할 필요가 없으나 현재 삭제할 필요가 없는 파일 저장  
        
	4) utils  
		- 노트북에서 재사용하는 다양한 모듈을 정리하는 공간  
```

3. Naming Rules  

```
    1) 폴더명  
        - 소문자 사용  
		- space 없이, _를 사용  
        
	2) 파일명  
		- 소문자 사용  
		- space 없이, _를 사용  
        
	3) 노트북 파일명  
        - 예시: 01_01_house_preprocessing_matt.ipynb  
		- 기능 구분: 앞의 숫자 2자리는 노트북 산출물 관점에서 보았을 때 노트북의 기능 번호를 나타내도록  
        
			* 예시  
			- 01: 데이터 전처리  
			- 02: EDA  
			- 03: Feature engineering  
			- 04: 모델링  
			- 05: Hyper parameter tuning  
			- ...  
            
		- 버전 구분: 뒤의 숫자 2자리는 노트북의 버전을 나타냄  
        
			* 예시  
			- 01: 1번째 버전  
			- 02: 2번째 버전  
            - ...  
            
		- 작성자 구분: 작성자의 이니셜 혹은 영문 이름을 소문자로 마지막에 기록함으로써 소유주 표시  
        
			* 예시  
			- ssc  
			- phj
            - sean
            - matt (이명학)
			- ...  
```          
