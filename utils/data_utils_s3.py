import pickle
import boto3
from io import BytesIO
from botocore.exceptions import NoCredentialsError
import pandas as pd


class local_S3_data_toolkit:
    
    def __init__(self, BUCKET_NAME, ACCESS_KEY, SECRET_KEY):

        self.BUCKET_NAME = BUCKET_NAME
    
        self.s3_client = boto3.client('s3',
                              aws_access_key_id=ACCESS_KEY,
                              aws_secret_access_key=SECRET_KEY)
        
        self.s3_resource = boto3.resource('s3',
                              aws_access_key_id=ACCESS_KEY,
                              aws_secret_access_key=SECRET_KEY)
        
        # self.my_bucket = resource.Bucket(self.BUCKET_NAME)
        
        
    def upload_to_S3(self, local_dir, s3_dir):
        try:
            self.s3_client.upload_file(local_dir, self.BUCKET_NAME, s3_dir)
            print("Upload Successful")
            return True
        except FileNotFoundError:
            print("The file was not found")
            return False
        except NoCredentialsError:
            print("Credentials not available")
            return False 
    
    def load_from_S3(self, s3_dir):
        if s3_dir.endswith(".csv"):
            obj = self.s3_client.get_object(Bucket=self.BUCKET_NAME, Key=s3_dir)
            loaded_csv= pd.read_csv(obj['Body'])
            return loaded_csv
        
        elif s3_dir.endswith(".pkl"):
            with BytesIO() as data:
                self.s3_resource.Bucket(self.BUCKET_NAME).download_fileobj(s3_dir, data)
                data.seek(0)    # move back to the beginning after writing
                loaded_pkl = pickle.load(data)
                
            return loaded_pkl
        
    